#!/bin/bash
primes='148459189 161345243 163013633 174239869 180661463 186833947 190465669 209078437 209434937 223790561 225916657 226218431 227922041 228304249 232178489 239125267 243483733 243585883 253274447 267177773'

nr=$1;
nprimes=$(( (2*$nr)/28 +1 ));

printf "KATAN32 -- nr: %3d, #primes: %2d\n" $nr $nprimes

i=0;

for p in $primes; do
     file=`printf "katan32-%03d-%d.dat" $nr $p`;
     printf "i: %2d, p: %10d, filename: %s\n" $i $p  $file;
     time ./m/markov_katan32 -r $nr -m $p -f $file
     echo;
     echo;
     i=$(($i + 1));
     if [ $i -ge $nprimes ]; then
         break;
     fi
done