from sage.all import *

#ctypedef unsigned int int_type
ctypedef unsigned long long int_type

cdef extern from "stdlib.h":
    void *calloc(size_t nmemb, size_t size)
    void free(void *ptr)

cdef extern from "stdio.h":
    ctypedef struct FILE "FILE":
        pass

    FILE *fopen(char *path, char *mode)
    int fclose(FILE *fp)
    size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream)
    size_t fwrite(void *ptr, size_t size, size_t nmemb, FILE *stream)

cdef extern from "math.h":
    long double logl(long double x)


def human_walltime(t):
    h = "%3d"%(floor(t/3600.0))
    m = "%2d"%floor((t%3600.0)/60.0)
    s = "%02d"%floor(t%60.0)
    return "%sh %sm:%ss"%(h,m,s)

def postprocess(files_p, nr, log_factor, N, filename_prefix="out"):
    cdef int_type tmp
    cdef long i
    cdef int np = len(files_p)

    normalise = ZZ(1)<<(nr*log_factor)
    cdef FILE **_files_p = <FILE **>calloc(np,sizeof(FILE *))
    filename = "%s-%03d-p.dat"%(filename_prefix,nr)

    cdef FILE *out = fopen(filename,"w")

    cdef list primes = []
    for i in range(np):
        _files_p[i] = fopen(files_p[i],'r')
        fread(&tmp, sizeof(int_type), 1, _files_p[i])
        primes.append(ZZ(tmp))

    basis = CRT_basis(primes)
    modulus = misc.prod(primes)

    print primes

    cdef long double pi, log_pi, log_1_minus_pi

    t = walltime()

    for i in xrange(N):
        p = 0
        for j in range(np):
            fread(&tmp, sizeof(int_type), 1, _files_p[j])
            p += (basis[j]*tmp)
        p = p%modulus
        pi = p/normalise

        fwrite(&pi, sizeof(long double), 1, out)

        if i%4096 == 0:
            T = walltime(t)
            print "i: 0x%08x, time: %s, remaining time: %s"%(i,
                                                             human_walltime(T), 
                                                             human_walltime((N-i-1)*(T/(i+1))))
    for i in range(np):
        fclose(_files_p[i])
    fclose(out)
    free(_files_p)

    return filename

def analyse(filename, N,int list_length = 1024):
    """
    INPUT:
    
    - ``N`` - the length of the vector
    """
    cdef long i = 0

    cdef long double wi
    cdef long double pi, log_1_minus_pi, log_pi
    cdef long double qi = float(ZZ(1)/(N-1))
    cdef long double log_qi = logl(qi)
    cdef long double log_1_minus_qi = logl(1-qi)
    cdef long double n = N/2

    cdef long double VR = 0.0
    cdef long double ER = 0.0
    cdef long double VW = 0.0
    cdef long double EW = 0.0


    cdef list u = []
    cdef list l = []

    cdef long double u_l = 0.0
    cdef long double l_l = 1.0


    cdef FILE *fh = fopen(filename,"r")

    fread(&pi, sizeof(long double), 1, fh)

    t = walltime()
    for i in xrange(1,N):

        fread(&pi, sizeof(long double), 1, fh)

        if pi > u_l:
            u.append( (pi,i) )
            u.sort(reverse=True)
            if len(u) > list_length:
                _ = u.pop()
            u_l = u[-1][0]

        if pi < l_l:
            l.append( (pi,i) )
            l.sort()
            if len(l) > list_length:
                _ = l.pop()
            l_l = l[-1][0]

        log_pi = logl(pi)
        log_1_minus_pi = logl(1-pi)

        wi = (log_pi  - log_qi - log_1_minus_pi + log_1_minus_qi)

        ER += n * wi * pi
        VR += n * wi**2 * pi * (1-pi)

        EW += n * wi * qi 
        VW += n * wi**2 * qi * (1-qi)

        if i%4096 == 0:
            T = walltime(t)
            gain = -log_b(RR(1 - ( 0.5 * (1 + erf( (ER - EW)/(VW**0.5 * 2.0**0.5) ) ))), 2.0)

            print "i: 0x%08x, Pr(0x%08x) = 2^%.10f, Gain: %10.7f, E(R): %12.7f, V(R): %12.7f, E(W): %12.7f, V(W): %12.7f, time: %s, remaining time: %s"%(i,
                                                                                                                                                   u[0][1],
                                                                                                                                                   log_b(u[0][0],2.0),
                                                                                                                                                   gain,                                                                                                                         
                                                                                                                                                   ER,
                                                                                                                                                   VR,
                                                                                                                                                   EW,
                                                                                                                                                   VW,
                                                                                                                                                   human_walltime(T), 
                                                                                                                                                   human_walltime((N-i-1)*(T/(i+1))))
    T = walltime(t)
    gain = -log_b(RR(1 - ( 0.5 * (1 + erf( (ER - EW)/(VW**0.5 * 2.0**0.5) ) ))), 2.0)

    print "i: 0x%08x, Pr(0x%08x) = 2^%.10f, Gain: %10.7f, E(R): %12.7f, V(R): %12.7f, E(W): %12.7f, V(W): %12.7f, time: %s, remaining time: %s"%(i,
                                                                                                                                                 u[0][1],
                                                                                                                                                 log_b(u[0][0],2.0),
                                                                                                                                                 gain,                                                                                                                         
                                                                                                                                                 ER,
                                                                                                                                                 VR,
                                                                                                                                                 EW,
                                                                                                                                                 VW,
                                                                                                                                                 human_walltime(T), 
                                                                                                                                                 human_walltime((N-i-1)*(T/(i+1))))


    fh1 = open(filename+"-lowest","w")
    for p,i in l:
        fh1.write("%08x: %10.7f\n"%(i,log_b(p,2)))
    fh1.close()

    fh1 = open(filename+"-highest","w")
    for p,i in u:
        fh1.write("%08x: %10.7f\n"%(i,log_b(p,2)))
    fh1.close()
    
    fclose(fh)
    #return l
