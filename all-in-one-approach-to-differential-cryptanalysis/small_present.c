/**
 * @author: Gregor Leander <G.Leander@mat.dtu.dk>
 * @author: Martin Albrecht <martinralbrecht@googlemail.com>
 */
#include "small_present.h"
#include <math.h>

/************************************************
 * Permutation
 ************************************************/

state_t permutation[64]={0x0000000000000001ull, 0x0000000000010000ull, 0x0000000100000000ull, 0x0001000000000000ull,
                         0x0000000000000002ull, 0x0000000000020000ull, 0x0000000200000000ull, 0x0002000000000000ull,
                         0x0000000000000004ull, 0x0000000000040000ull, 0x0000000400000000ull, 0x0004000000000000ull,
                         0x0000000000000008ull, 0x0000000000080000ull, 0x0000000800000000ull, 0x0008000000000000ull,
                         0x0000000000000010ull, 0x0000000000100000ull, 0x0000001000000000ull, 0x0010000000000000ull,
                         0x0000000000000020ull, 0x0000000000200000ull, 0x0000002000000000ull, 0x0020000000000000ull,
                         0x0000000000000040ull, 0x0000000000400000ull, 0x0000004000000000ull, 0x0040000000000000ull,
                         0x0000000000000080ull, 0x0000000000800000ull, 0x0000008000000000ull, 0x0080000000000000ull,
                         0x0000000000000100ull, 0x0000000001000000ull, 0x0000010000000000ull, 0x0100000000000000ull,
                         0x0000000000000200ull, 0x0000000002000000ull, 0x0000020000000000ull, 0x0200000000000000ull,
                         0x0000000000000400ull, 0x0000000004000000ull, 0x0000040000000000ull, 0x0400000000000000ull,
                         0x0000000000000800ull, 0x0000000008000000ull, 0x0000080000000000ull, 0x0800000000000000ull,
                         0x0000000000001000ull, 0x0000000010000000ull, 0x0000100000000000ull, 0x1000000000000000ull,
                         0x0000000000002000ull, 0x0000000020000000ull, 0x0000200000000000ull, 0x2000000000000000ull,
                         0x0000000000004000ull, 0x0000000040000000ull, 0x0000400000000000ull, 0x4000000000000000ull,
                         0x0000000000008000ull, 0x0000000080000000ull, 0x0000800000000000ull, 0x8000000000000000ull};

state_t P[16][16];

state_t permutations[17][64];

void generate_permutations() {
  for (int n=2;n<=16;n++) {
    for (int i=0;i<4*n;i++) {
      int j=(n*i)%(4*n-1);
      if (i==4*n-1)
        j=4*n-1;
      permutations[n][i]=(1ull)<<j;
    }
  }
}

/*
 * code for generating the permutation as a total of n table lookups
 */

void generate_permutation_lookups(int n) {
  for(int i=0;i<16;i++) {
    for(int j=0;j<16;j++) {
      P[i][j]=0;
    }
  }

  state_t b[4] = {0,0,0,0};
  state_t *_p = permutations[n];

  for(int j=0;j<n;j++) {
    for(int i=0;i<16;i++) {
      b[0] = ((i&1)>>0) * _p[0+4*j];
      b[1] = ((i&2)>>1) * _p[1+4*j];
      b[2] = ((i&4)>>2) * _p[2+4*j];
      b[3] = ((i&8)>>3) * _p[3+4*j];

      P[j][i] = b[0]^b[1]^b[2]^b[3];
    }
  }
}

/************************************************
 * Substitution
 ************************************************/

state_t SBox[16]={0xcull,0x5ull,0x6ull,0xbull,
                  0x9ull,0x0ull,0xaull,0xdull,
                  0x3ull,0xeull,0xfull,0x8ull,
                  0x4ull,0x7ull,0x1ull,0x2ull};

struct sbox_diff SD[] = { /* we pad everything to eight elements */
  {1, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, {8, 0, 0, 0, 0, 0, 0, 0}},
  {4, {0x3, 0x7, 0x9, 0xd, 0x0, 0x0, 0x0, 0x0}, {2, 2, 2, 2, 0, 0, 0, 0}},
  {7, {0x3, 0x5, 0x6, 0xa, 0xc, 0xd, 0xe, 0x0}, {1, 2, 1, 1, 1, 1, 1, 0}},
  {7, {0x1, 0x3, 0x4, 0x6, 0x7, 0xa, 0xb, 0x0}, {1, 1, 1, 2, 1, 1, 1, 0}},
  {7, {0x5, 0x6, 0x7, 0x9, 0xa, 0xc, 0xe, 0x0}, {2, 1, 1, 1, 1, 1, 1, 0}},
  {7, {0x1, 0x4, 0x9, 0xa, 0xb, 0xc, 0xd, 0x0}, {1, 1, 1, 1, 1, 2, 1, 0}},
  {6, {0x2, 0x6, 0x8, 0xb, 0xc, 0xf, 0x0, 0x0}, {1, 1, 1, 2, 1, 2, 0, 0}},
  {6, {0x1, 0x2, 0x6, 0x8, 0xc, 0xf, 0x0, 0x0}, {2, 1, 1, 1, 1, 2, 0, 0}},
  {6, {0x3, 0x7, 0x9, 0xb, 0xd, 0xf, 0x0, 0x0}, {1, 1, 1, 2, 1, 2, 0, 0}},
  {6, {0x2, 0x4, 0x6, 0x8, 0xc, 0xe, 0x0, 0x0}, {1, 2, 1, 1, 1, 2, 0, 0}},
  {7, {0x2, 0x3, 0x5, 0x8, 0xa, 0xd, 0xe, 0x0}, {1, 1, 2, 1, 1, 1, 1, 0}},
  {7, {0x1, 0x4, 0x8, 0x9, 0xa, 0xb, 0xd, 0x0}, {1, 1, 2, 1, 1, 1, 1, 0}},
  {7, {0x2, 0x5, 0x7, 0x8, 0x9, 0xa, 0xe, 0x0}, {1, 2, 1, 1, 1, 1, 1, 0}},
  {7, {0x1, 0x2, 0x3, 0x4, 0x7, 0xa, 0xb, 0x0}, {1, 2, 1, 1, 1, 1, 1, 0}},
  {8, {0x2, 0x3, 0x6, 0x7, 0x8, 0x9, 0xc, 0xd}, {1, 1, 1, 1, 1, 1, 1, 1}},
  {4, {0x1, 0x4, 0xe, 0xf, 0x0, 0x0, 0x0, 0x0}, {2, 2, 2, 2, 0, 0, 0, 0}},
};

struct sbox_diff ISD[] = {
  {1, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, {8, 0, 0, 0, 0, 0, 0, 0}},
  {6, {0x3, 0x5, 0x7, 0xb, 0xd, 0xf, 0x0, 0x0}, {1, 1, 2, 1, 1, 2, 0, 0}},
  {7, {0x6, 0x7, 0x9, 0xa, 0xc, 0xd, 0xe, 0x0}, {1, 1, 1, 1, 1, 2, 1, 0}},
  {7, {0x1, 0x2, 0x3, 0x8, 0xa, 0xd, 0xe, 0x0}, {2, 1, 1, 1, 1, 1, 1, 0}},
  {6, {0x3, 0x5, 0x9, 0xb, 0xd, 0xf, 0x0, 0x0}, {1, 1, 2, 1, 1, 2, 0, 0}},
  {4, {0x2, 0x4, 0xa, 0xc, 0x0, 0x0, 0x0, 0x0}, {2, 2, 2, 2, 0, 0, 0, 0}},
  {7, {0x2, 0x3, 0x4, 0x6, 0x7, 0x9, 0xe, 0x0}, {1, 2, 1, 1, 1, 1, 1, 0}},
  {7, {0x1, 0x3, 0x4, 0x8, 0xc, 0xd, 0xe, 0x0}, {2, 1, 1, 1, 1, 1, 1, 0}},
  {7, {0x6, 0x7, 0x9, 0xa, 0xb, 0xc, 0xe, 0x0}, {1, 1, 1, 1, 2, 1, 1, 0}},
  {7, {0x1, 0x4, 0x5, 0x8, 0xb, 0xc, 0xe, 0x0}, {2, 1, 1, 1, 1, 1, 1, 0}},
  {8, {0x2, 0x3, 0x4, 0x5, 0xa, 0xb, 0xc, 0xd}, {1, 1, 1, 1, 1, 1, 1, 1}},
  {6, {0x3, 0x5, 0x6, 0x8, 0xb, 0xd, 0x0, 0x0}, {1, 1, 2, 2, 1, 1, 0, 0}},
  {7, {0x2, 0x4, 0x5, 0x6, 0x7, 0x9, 0xe, 0x0}, {1, 1, 2, 1, 1, 1, 1, 0}},
  {7, {0x1, 0x2, 0x5, 0x8, 0xa, 0xb, 0xe, 0x0}, {2, 1, 1, 1, 1, 1, 1, 0}},
  {6, {0x2, 0x4, 0x9, 0xa, 0xc, 0xf, 0x0, 0x0}, {1, 1, 2, 1, 1, 2, 0, 0}},
  {4, {0x6, 0x7, 0x8, 0xf, 0x0, 0x0, 0x0, 0x0}, {2, 2, 2, 2, 0, 0, 0, 0}},
};

state_t S[16][16];

void generate_super_sboxes(int n) {
  //code for generating the S-box and permutation as a total of n table lookups
  for(int i=0;i<16;i++) {
    for(int j=0;j<16;j++) {
      S[i][j]=0ull;
    }
  }

  state_t bit0=0;
  state_t bit1=0;
  state_t bit2=0;
  state_t bit3=0;

  for(int j=0;j<n;j++){
    for(int i=0;i<16;i++){
      bit0=0;
      bit1=0;
      bit2=0;
      bit3=0;
      if((SBox[i]&1)==1)
        bit0 = permutations[n][0+4*j];
      if(((SBox[i]>>1)&1)==1)
        bit1 = permutations[n][1+4*j];
      if(((SBox[i]>>2)&1)==1)
        bit2 = permutations[n][2+4*j];
      if(((SBox[i]>>3)&1)==1)
        bit3 = permutations[n][3+4*j];
      state_t sum = bit0^bit1^bit2^bit3;
      S[j][i]=sum;
    }
  }
}

/************************************************
 * Encryption
 ************************************************/

state_t *cipher_round;
state_t *cipher_inv_round;

static inline state_t encrypt_lookup(state_t plaintext, state_t *keystate, int n, int nr) {
  state_t state = plaintext;

  switch(nr) {
  default:
    printf("NotImplementedError\n");
    exit(1);
    break;
  case 16: state = cipher_round[(state ^ keystate[nr-16]) &0xffff];
  case 15: state = cipher_round[(state ^ keystate[nr-15]) &0xffff];
  case 14: state = cipher_round[(state ^ keystate[nr-14]) &0xffff];
  case 13: state = cipher_round[(state ^ keystate[nr-13]) &0xffff];
  case 12: state = cipher_round[(state ^ keystate[nr-12]) &0xffff];
  case 11: state = cipher_round[(state ^ keystate[nr-11]) &0xffff];
  case 10: state = cipher_round[(state ^ keystate[nr-10]) &0xffff];
  case  9: state = cipher_round[(state ^ keystate[nr- 9]) &0xffff];
  case  8: state = cipher_round[(state ^ keystate[nr- 8]) &0xffff];
  case  7: state = cipher_round[(state ^ keystate[nr- 7]) &0xffff];
  case  6: state = cipher_round[(state ^ keystate[nr- 6]) &0xffff];
  case  5: state = cipher_round[(state ^ keystate[nr- 5]) &0xffff];
  case  4: state = cipher_round[(state ^ keystate[nr- 4]) &0xffff];
  case  3: state = cipher_round[(state ^ keystate[nr- 3]) &0xffff];
  case  2: state = cipher_round[(state ^ keystate[nr- 2]) &0xffff];
  case  1: state = cipher_round[(state ^ keystate[nr- 1]) &0xffff];
  case  0: state = (state ^ keystate[nr- 0]) &0xffff;
  }
  return state;
}

/************************************************
 * Utility
 ************************************************/

void print_diff(diff_t state,int n) {
  if (n==2) printf("%02zx",state);
  if (n==3) printf("%03zx",state);
  if (n==4) printf("%04zx",state);
  if (n==5) printf("%05zx",state);
  if (n==6) printf("%06zx",state);
  if (n==7) printf("%07zx",state);
  if (n==8) printf("%08zx",state);
  if (n==9) printf("%09zx",state);
  if (n==10) printf("%010zx",state);
  if (n==11) printf("%011zx",state);
  if (n==12) printf("%012zx",state);
  if (n==13) printf("%013zx",state);
  if (n==14) printf("%014zx",state);
  if (n==15) printf("%015zx",state);
  if (n==16) printf("%016zx",state);
}

double walltime( double t0 ) {
  double mic, time;
  double mega = 0.000001;
  struct timeval tp;
  static long base_sec = 0;
  static long base_usec = 0;

  (void) gettimeofday(&tp,NULL);
  if (base_sec == 0)
    {
      base_sec = tp.tv_sec;
      base_usec = tp.tv_usec;
    }

  time = (double) (tp.tv_sec - base_sec);
  mic = (double) (tp.tv_usec - base_usec);
  time = (time + mic * mega) - t0;
  return(time);
}

//initialize all tables

void init(int n) {
  state_t stateA, ciphertext;

  generate_permutations();
  generate_permutation_lookups(n);
  generate_super_sboxes(n);

  cipher_round     = (state_t*)calloc(1<<16,sizeof(state_t));
  cipher_inv_round = (state_t*)calloc(1<<16,sizeof(state_t));

  for (stateA=0; stateA<=0xffff; stateA++) {
    ciphertext = 0;
    for(int i=0; i<4; i++){
      ciphertext= ciphertext ^ S[i][((stateA>>4*i)&0xf)];
    }
    cipher_round[stateA] = ciphertext&0xffff;
    cipher_inv_round[(ciphertext)&0xffff] = stateA;
  }
}

void fini(int n) {
  free(cipher_round);
  free(cipher_inv_round);
}

/************************************************
 * Markov Round
 ************************************************/

void round4(diff_t *tmp, diff_t * data, uint64_t modulus) {

  uint64_t max_value = 0;

  for(diff_t d_in = 0; d_in < (1<<16); d_in++) {

    /* apply S-box layer */
    struct sbox_diff s0 = SD[((d_in>> 0)&0xf)];
    struct sbox_diff s1 = SD[((d_in>> 4)&0xf)];
    struct sbox_diff s2 = SD[((d_in>> 8)&0xf)];
    struct sbox_diff s3 = SD[((d_in>>12)&0xf)];

    if(data[d_in]) {
      for(int i0=0; i0<s0.length; i0++) {
        for(int i1=0; i1<s1.length; i1++) {
          for(int i2=0; i2<s2.length; i2++) {
            for(int i3=0; i3<s3.length; i3++) {
              diff_t d_out = (s3.x[i3]<<12) | (s2.x[i2]<<8) | (s1.x[i1]<<4) | (s0.x[i0]<<0);
              tmp[d_out] =  (tmp[d_out] + s3.p[i3] * s2.p[i2] * s1.p[i1] * s0.p[i0] * data[d_in]);
            }
          }
        }
      }
    }
  }

  for(diff_t d_in = 0; d_in < (1<<16); d_in++) {
    diff_t d_out = 0;

    d_out ^= P[0][((d_in>> 0)&0xf)];
    d_out ^= P[1][((d_in>> 4)&0xf)];
    d_out ^= P[2][((d_in>> 8)&0xf)];
    d_out ^= P[3][((d_in>>12)&0xf)];

    tmp[d_in] = tmp[d_in] % modulus;
    data[d_out] = tmp[d_in];

    if (tmp[d_in] > max_value) {
      max_value = tmp[d_in];
    }
    tmp[d_in] = 0;
  }
}

void round4_wrong_dec(diff_t *tmp, diff_t * data, uint64_t modulus) {
  for(diff_t d_in = 0; d_in < (1<<16); d_in++) {
    diff_t d_out = 0;

    d_out ^= P[0][((d_in>> 0)&0xf)];
    d_out ^= P[1][((d_in>> 4)&0xf)];
    d_out ^= P[2][((d_in>> 8)&0xf)];
    d_out ^= P[3][((d_in>>12)&0xf)];

    tmp[d_in] = data[d_out];
    data[d_out]  = 0;
  }

  for(diff_t d_in = 0; d_in < (1<<16); d_in++) {
    /* apply S-box layer */
    struct sbox_diff s0 = ISD[((d_in>> 0)&0xf)];
    struct sbox_diff s1 = ISD[((d_in>> 4)&0xf)];
    struct sbox_diff s2 = ISD[((d_in>> 8)&0xf)];
    struct sbox_diff s3 = ISD[((d_in>>12)&0xf)];

    if(tmp[d_in]) {
      for(int i0=0; i0<s0.length; i0++) {
        for(int i1=0; i1<s1.length; i1++) {
          for(int i2=0; i2<s2.length; i2++) {
            for(int i3=0; i3<s3.length; i3++) {
              diff_t d_out = (s3.x[i3]<<12) | (s2.x[i2]<<8) | (s1.x[i1]<<4) | (s0.x[i0]<<0);
              data[d_out] =  ((data[d_out] + s3.p[i3] * s2.p[i2] * s1.p[i1] * s0.p[i0] * tmp[d_in])) % modulus;
            }
          }
        }
      }
    }
  }
}


/************************************************
 * Attack
 ************************************************/

void markov_postprocess(int n, int nr, diff_t *markov, long double *markov_log_p, long double *markov_log_1_minus_p) {
  int const twopow = (1<<n);
  int const normalise = nr*12;

  for (int i=1; i<(1<<twopow); i++) {
    if (markov[i]!=0) {
      long double temp = (long double)markov[i];
      long double p = temp/(long double)(1ull<<normalise);
      long double logP = logl(p);
      long double logPMinus1 = logl(1-p);

      markov_log_p[i] = logP;
      markov_log_1_minus_p[i] = logPMinus1;

    } else {
      markov_log_p[i] = logl(0);
      markov_log_1_minus_p[i] = 0.0;
    }
  }
}

long double compute_prob(int nb, diff_t *diffs_out, long double *log_p, long double *log_q) {
  int const twopow = (1<<nb);
  long double log_prob = 0.0;
  long double infty = logl(0);

  for (int i=1; i<(1<<twopow); i++) {
    if (log_p[i] != infty) {
      log_prob += (log_p[i] - log_q[i])* diffs_out[i];
    } else if (diffs_out[i]!=0) {
      log_prob += infty;
    }
  }
  return log_prob;
}

long double encrypt_and_compute_prob(int n, int nr, diff_t d_i, int key_guess, state_t *keystate,
                                     long double *log_p, long double *log_q) {
  state_t c0, c1;
  int const N = 1<<(1<<n);

  /* int limit = 1<<9; */

  diff_t *diffs_out = (diff_t*)calloc(1<<16,sizeof(diff_t));

  assert(d_i != 0);

  state_t filter = 0;
  for(int i=0;i<16; i++) {
    if (d_i & 1<<i) {
      filter = (1<<i);
      break;
    }
  }
  for (state_t i=0; i<N; i++) {
    if (i & filter) {
      continue;
    }
    c0 = encrypt_lookup(i, keystate, n, nr);
    c0 = cipher_inv_round[c0 ^ key_guess];

    c1 = encrypt_lookup(i^d_i, keystate, n, nr);
    c1 = cipher_inv_round[c1 ^ key_guess];
    diffs_out[c0 ^ c1]++;
    /* limit -= 1; */
    /* if (limit == 0) */
    /*   break; */
  }

  /* FILE *fh = fopen("diffs_out.dat","wb"); */
  /* fwrite(diffs_out,sizeof(diff_t),1<<16,fh); */
  /* fclose(fh); */

  long double prob = compute_prob(n, diffs_out, log_p, log_q);
  free(diffs_out);
  return prob;
}


diff_t *run_markov(int n, int nr, diff_t d_i, uint64_t modulus) {
  assert(n==4);

  diff_t *dif = (diff_t*)calloc(sizeof(uint64_t),(1<<16));
  diff_t *tmp = (diff_t*)calloc(sizeof(uint64_t),(1<<16));

  dif[d_i] = 1;

  for(int i=0; i<nr; i++) {
    round4(tmp,dif, modulus);
  }
  free(tmp);
  return dif;
}


int run_attack(int n, int nr, diff_t d_i,
               long double *markov_log_p, long double *markov_log_1_minus_p,
               long double *markov_log_q, long double *markov_log_1_minus_q,
               int seed) {
  state_t keystate[nr+1];
  int rank = 0;

  long double * key_probability = (long double*)calloc(1<<16,sizeof(long double));
  long double right_key_probability;
  int const experiments = 16;

  srandom(seed);

  for (int i=0; i<nr+1; i++) {
    keystate[i]=random()&0xffff;
  }

  //compute the right key prob first

  right_key_probability = encrypt_and_compute_prob(n, nr, d_i, keystate[nr], keystate,
                                                   markov_log_p, markov_log_q);
  printf("  right key prob: %20.15Lf\n",right_key_probability);

  for (int key_guess=0; key_guess<(1<<experiments); key_guess++) {
    key_probability[key_guess] = encrypt_and_compute_prob(n, nr, d_i, key_guess, keystate,
                                                          markov_log_p, markov_log_q);

    if (key_guess != keystate[nr] && right_key_probability <= key_probability[key_guess]) {
      rank++;
    }

    if (key_guess%1000==0) {
      printf("\r 0x%04x key prob: %20.15Lf, right key rank: %5d", key_guess, key_probability[key_guess], rank);
      fflush(stdout);
    }
  }

  printf("\n rank of the right key: %5d (0 is best)\n",rank);

  free(key_probability);
  return rank;
}
