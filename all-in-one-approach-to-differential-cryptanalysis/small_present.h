/**
 * @author: Gregor Leander <G.Leander@mat.dtu.dk>
 * @author: Martin Albrecht <martinralbrecht@googlemail.com>
 */

#include <time.h>
#include <sys/time.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef uint64_t diff_t;
typedef uint64_t state_t;

extern uint64_t permutation[64];
extern uint64_t P[16][16];
extern uint64_t permutations[17][64];

void init(int n);

void generate_permutations(void);
void generate_permutation_lookups(int n);

struct sbox_diff {
  size_t length;
  diff_t x[8];
  uint64_t p[8];
};

extern struct sbox_diff SD[];

void print_diff(diff_t state,int n);
double walltime( double t0 );

void round4(diff_t *tmp, diff_t * data, uint64_t modulus);
void round4_wrong_dec(diff_t *out, diff_t * in, uint64_t modulus);

diff_t *run_markov(int n, int nr, diff_t d_i, uint64_t modulus);
void markov_postprocess(int n, int nr, diff_t *markov, long double *markov_log_p, long double *markov_log_1_minus_p);

long double encrypt_and_compute_prob(int n, int nr, diff_t d_i, int key_guess, state_t *keystate, 
                                     long double *markov_log_p, long double *markov_log_q);

int run_attack(int n, int nr, diff_t d_i, 
               long double *markov_log_p, long double *markov_log_1_minus_p, 
               long double *markov_log_q, long double *markov_log_1_minus_q,
               int seed);



