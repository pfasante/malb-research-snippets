# -*- coding: utf-8 -*-
"""
Parameter estimation for the BKW algorithm on LWE instances based on [ACFFP12]_.

AUTHOR:

- Martin Albrecht - 2012 - initial implementation

REFERENCES:

.. [ACFFP12] Martin R. Albrecht, Carlos Cid, Jean-Charles Faugère, Robert Fitzpatrick and Ludovic
   Perret. On the Complexity of the BKW Algorithm on LWE in Cryptology ePrint Archive: Report
   2012/636, 2012. http://eprint.iacr.org/2012/636
"""
import sys
import pylab

class BKW:
    def __init__(self, n, t=2.0, q=None, alpha=None, d=2, prec=None):
        """
        INPUT:

        - ``n``            - the number of variables in the LWE instance
        - ``t``            - the target number of additions is t*log2(n)
        - ``q``            - the size of the finite field (default: n^2)
        - ``alpha``        - the standard deviation of the LWE instance is alpha*q (default: 1/(sqrt(n)*log^2(n)))
        - ``d``            - the number of elements targeted in hypothesis testing (default: 2)
        - ``prec``         - bit precision used for numerical computations (default: 2*n)
        """
        self.prec = 2*n if prec is None else prec
        RR        = RealField(self.prec)
        self.RR   = RR

        self.a = self.RR(t*log((n),2)) # the target number of additions: a = t*log_2(n)
        self.b = n/self.a # window width b = n/a

        q     = ZZ(next_prime(n**2)) if q is None else ZZ(q)
        alpha = 1/(RR(n).sqrt() * RR(log(n,2)**2)) if alpha is None else RR(alpha)
        sigma = RR(n**t).sqrt() * alpha*q # after n^t additions we get this stddev

        self.q     = q
        self.alpha = alpha
        self.sigma = sigma

        self.search_space = q

        self.n = n
        self.d = d
        self.t = t

        # we cache some constants that we use often
        self.lower_bound = -floor(q/2)
        self.upper_bound =  floor(q/2)
        self.one_half    =  self.RR(0.5)

        self.K = GF(self.q)

    def cdf(self, x, sigma, mu=0):
        # calling erf() on RealFieldElement is faster than the global erf() function
        try:
            return self.one_half * (1 + ( (x-mu) / (2 * sigma**2 ).sqrt() ).erf() )
        except AttributeError:
            return self.one_half * (1 + erf( (x-mu) / (2 * sigma**2 ).sqrt() ) )

    def compute_p(self, q, sigma):
        oh = self.one_half

        p = {}
        for j in xrange(self.lower_bound, self.upper_bound+1):
            p[j] = self.cdf(j+oh, sigma).n(prec=self.prec) - self.cdf(j-oh, sigma).n(prec=self.prec)

        old_sum = sum(p.values())
        for i in range(1,self.n**2):
            for j in range(self.lower_bound, self.upper_bound+1):
                p[j] += ( self.cdf(  q*i +j + oh, sigma).n(prec=self.prec) - self.cdf(  q*i + j - oh, sigma) ).n(prec=self.prec)
                p[j] += ( self.cdf( -q*i +j + oh, sigma).n(prec=self.prec) - self.cdf( -q*i + j - oh, sigma) ).n(prec=self.prec)
            if get_verbose() >= 1:
                print("i: %3d, sum(p) = %s"%(i, sum(p.values()).str()))
                sys.stdout.flush()
            if  sum(p.values()) == old_sum:
                break
            old_sum = sum(p.values())
        return p

    def m(self, m=None, success=0.99999, d=2):
        if get_verbose() >= 1:
            print "p_success", success
        p = self.p
        q = self.q

        w = {}
        r = {}
        E_c, E_w, V_c, V_w = self.RR(0), self.RR(0), self.RR(0), self.RR(0)

        for j in range(self.lower_bound, self.upper_bound+1):
            r[j] = (q**(d-1) - p[j])/(q**d - 1)
            w[j] = (log(p[j], 2) - log(r[j], 2)).n(prec=self.prec)
            E_c += w[j] * p[j]
            E_w += w[j] * r[j]

        for j in range(self.lower_bound, self.upper_bound+1):
            V_c += p[j] * (w[j] - E_c)**2
            V_w += r[j] * (w[j] - E_w)**2

        from mpmath import mp
        power = int(self.search_space**d - 1)
        if get_verbose() >= 1:
            print "exponent:", power
        mp.prec = self.prec

        def make_f(m):
            return lambda x: (0.5 * (1 + mp.erf( (x - E_w)/mp.sqrt(2*V_w/m) )))**power * 1/(mp.sqrt(2 * mp.pi * V_c/m)) * mp.exp( -(x-E_c)**2/(2*V_c/m) )

        if m is None:
            m = 2
            f = make_f(m)
            intgrl = mp.quad(f, [-mp.inf, mp.inf])
            while intgrl < success:
                m *= 10
                f = make_f(m)
                intgrl = mp.quad(f, [-mp.inf, mp.inf])
                if get_verbose() >= 1:
                    print "log_2(m): %6.2f, p_success: %s"%(log(m,2),intgrl)
                if m > 2**self.n:
                    raise OverflowError
            while intgrl > success:
                m /= 2.0
                f = make_f(m)
                intgrl = mp.quad(f, [-mp.inf, mp.inf])
                if get_verbose() >= 1:
                    print "log_2(m): %6.2f, p_success: %s"%(log(m,2),intgrl)
            m *= 2.0
        else:
            f = make_f(m)
            if get_verbose() >= 1:
                print "log_2(m): %6.2f, p_success: %s"%(log(m,2),mp.quad(f, [-mp.inf, mp.inf]))

        V_w = V_w/m
        V_c = V_c/m

        if get_verbose() >= 1:
            print(" E_c: %.15f,  V_c: %.24f,  E_w: %.15f,  V_w: %.24f"%(E_c, V_c, E_w, V_w))

        return m

    def __repr__(self):
        return "n: %3d, q: %6d, alpha: %.10f, sigma: %.10f"%(self.n, self.q, self.alpha, self.sigma)


    def __getattr__(self, name):
        if name == "p":
            self.p = self.compute_p(self.q, self.sigma)
            return self.p
        else:
            raise AttributeError("Name '%s' unknown."%name)

    def estimate(self, success=0.99):
        """
        Estimate complexity

        INPUT:

        - ``success`` - target total success probability (default: ``0.99``)

        OUTPUT:

            n, t, success, d, log2(m), log2(stage1), log2(stage2), log2(stage3), log2(nrops), log2(nbops), log2(ncalls)
        """
        d = self.d
        q = self.q
        b = self.b
        a = self.a
        n = self.n

        repeat = ZZ(ceil(n/d))

        success = self.RR(success)

        m = ZZ(round(self.m(success = success**(1/repeat) )))

        corrector = (q**d)/(q**d - 1)

        stage1a = (q**b-1)/2.0 * ( a*(a-1)/2.0 * (n+1) - b*a*(a-1)/4.0 - b/6.0 * ( (a-1)**3 + 3/2.0*(a-1)**2 + 1/2.0*(a-1) ) )
        stage1b = corrector * (repeat + 1)/2.0 * m * (a/2.0 * (n + 2))

        stage1  = stage1a + stage1b
        stage2  = repeat * m * q**d
        stage3  = (repeat + 1) * d * a * ceil(q**b/2.0)

        nrops = stage1 + stage2 + stage3
        nbops = log(q,2)**2 * nrops

        ncalls = a * ceil(q**b/2.0) +  corrector * repeat * m

        return self.n, self.t, success, d, log(m,2).n(), log(stage1,2).n(), log(stage2,2).n(), log(stage3,2).n(), log(nrops,2).n(), log(nbops,2).n(), log(ncalls,2).n()

    def __call__(self, success=0.99, latex_output=False):
        """
        print estimated complexity

        INPUT:

        - ``success`` - target total success probability (default: ``0.99``)
        - ``latex_output`` - print as LaTeX table row (default: ``False``)
        """

        n, t, success, d, logm, logstage1, logstage2, logstage3, lognrops, lognbops, logncalls = self.estimate(success=success)

        if not latex_output:
            print "n: %3d, t: %3.1f, success: %7.5f, d: %2d,"%(n, t, success, d),
            print "log_2(m): %6.2f,"%logm,
            print "log_2(#sample): %6.2f,"%logstage1,
            print "log_2(#hypothesis): %6.2f,"%logstage2,
            print "log_2(#back): %6.2f,"%logstage3,
            print "log_2(#total): %6.2fs"%lognrops,
            print "log_2(#bops): %6.2f,"%lognbops,
            print "log_2(#Ldis): %6.2f"%logncalls
        else:
            print "%3d &"%self.n,
            print "%6.2f &"%logm,
            print "%6.2f &"%logstage1,
            print "%6.2f &"%logstage2,
            print "%6.2f &"%logstage3,
            print "%6.2f &"%lognrops,
            print "%6.2f &"%lognbops,
            print "%6.2f\\\\"%logncalls


    def print_parameters(self, success=0.99):
        """
        Print command line for BKW implementation.
        """
        repeat = ZZ(ceil(self.n/self.d))
        m = self.m(success = success**(1/repeat))
        print "./small -n %2d -q %4d -s %.2f -w %2d -m %.2f -v"%(self.n, self.q, self.alpha*self.q, ceil(self.b * log(self.q,2.0).n()), log(m,2.0))


# Micciancio & Regev 2009

pqc_params = [
    (136,  2003, 0.0065   , 2.1),
    (166,  4093, 0.0024   , 2.4),
    (192,  8191, 0.0009959, 2.7),
    (214, 16381, 0.00045  , 3.0),
    (233, 32749, 0.000217 , 3.2)]

# Albrecht et al. 2011

pollycracker_params = [
    ( 136,         1999,  0.00558254200346408, 2.2),
    ( 231,        92893, 0.000139563550086602, 3.4),
    ( 153,        12227,  0.00279740858078175, 2.4),
    ( 253,       594397,0.0000349676072597719, 3.8),]

def call_on_params(p, success=0.99, d=2):
    n,q,alpha,t = p
    BKW(n=n, q=q, alpha=alpha,t=t,d=d)(success=success, latex_output=True)
