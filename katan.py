"""
KATAN & KTANTAN

AUTHOR: Martin Albrecht <M.R.Albrecht@rhul.ac.uk>
"""
from sage.crypto.mq.sbox import SBox
from sage.crypto.mq.mpolynomialsystemgenerator import MPolynomialSystemGenerator
from sage.crypto.mq.mpolynomialsystem import MPolynomialSystem, MPolynomialRoundSystem

from sage.misc.functional import parent
from sage.misc.cachefunc import cached_function
from sage.modules.free_module import VectorSpace, FreeModule

from sage.rings.all import FiniteField as GF
from sage.rings.integer_ring import ZZ
from sage.rings.polynomial.polynomial_ring_constructor import BooleanPolynomialRing_constructor as BooleanPolynomialRing
from sage.rings.polynomial.pbori import BooleanPolynomialRing as BooleanPolynomialRing_class

from sage.structure.sage_object import SageObject

def whoami():
    """
    Print some helpful information about which revisions and date.

    EXAMPLE:
        sage: whoami() # output random
        r17 M | road | 2009-02-01 17:18
    """
    import commands, time
    revision = 0
    for line in commands.getoutput("hg tip").splitlines():
        if "changeset" in line:
            revision =  int(line.split(":")[1])
            break
    try:
        status = commands.getoutput("hg status %s"%__file__)[0]
    except (IndexError, NameError):
        status = ""
    hostname = commands.getoutput("uname -n")
    date = time.localtime()
    date = "%04d-%02d-%02d %02d:%02d"%(date[0],date[1],date[2],date[3],date[4])

    print "r%d"%revision, status, "|", hostname, "|", date

whoami()

class KATAN(MPolynomialSystemGenerator):
    """
    Base class for all KATAN and KTANTAN variants.
    """
    def __init__(self, Nr=254, **kwds):
        """
        EXAMPLE::

            sage: KATAN32(Nr=10)
            KATAN32-10
        """
        if Nr < 1 or Nr>254:
            raise ValueError("Number of rounds must be >= 1 and <= 254.")
        self.Nr = Nr
        KATAN = self.__class__

        self._order = kwds.get('order','deglex')
        self._postfix = kwds.get("postfix","")
        if self.Bs == 32:
            self.nvars = 2*self.Nr
        elif self.Bs == 48:
            self.nvars = 2*2*self.Nr
        elif self.Bs == 64:
            self.nvars = 3*2*self.Nr

    def new_generator(self, **kwds):
        """
        EXAMPLE::

            sage: KTN = KATAN32(Nr=10)
            sage: KTN.new_generator(Nr=12)
            KATAN32-12
        """ 

        KATAN = self.__class__

        Nr = kwds.get("Nr",self.Nr)
        order = kwds.get("order", self._order)
        postfix = kwds.get("postfix",self._postfix)
        
        return KATAN(Nr=Nr,order=order,postfix=postfix)

    def __repr__(self):
        """
        EXAMPLE::

            sage: KATAN32(Nr=10) # indirect doctest
            KATAN32-10
        """
        return "KATAN%d-%d"%(self.__class__.Bs,self.Nr)

    def __getattr__(self, attr):
        """
        EXAMPLE::

            sage: KTN = KATAN32(Nr=10)
            sage: KTN.R # indirect doctest
            Boolean PolynomialRing in Y19, Y18, Y17, Y16, Y15, Y14, Y13, Y12, Y11, Y10, Y09, Y08, Y07, Y06, Y05, Y04, Y03, Y02, Y01, Y00, 
            K00, K01, K02, K03, K04, K05, K06, K07, K08, K09, K10, K11, K12, K13, K14, K15, K16, K17, K18, K19, K20, K21, K22, K23, 
            K24, K25, K26, K27, K28, K29, K30, K31, K32, K33, K34, K35, K36, K37, K38, K39, K40, K41, K42, K43, K44, K45, K46, K47, 
            K48, K49, K50, K51, K52, K53, K54, K55, K56, K57, K58, K59, K60, K61, K62, K63, K64, K65, K66, K67, K68, K69, K70, K71, 
            K72, K73, K74, K75, K76, K77, K78, K79
        """
        if attr == "R":
            self.R = self.ring()
            return self.R
        raise AttributeError("'%s' object has no attribute '%s'"%(self.__class__,attr))

    @staticmethod
    def str_to_dc(s):
        L2,L1 = s.split(' ')
        L2,L1= list(reversed(L2)),list(reversed(L1))
        return tuple(map(int,L2 + L1))

    @staticmethod
    def bitstringtohexstring(l):
        """
        The bit order is big endian.

        EXAMPLE::

            sage: KATAN.bitstringtohexstring([0,0,0,1, 1,1,0,0])
            '1c'
        """
        r = []
        for i in xrange(0,len(l),4):
            z = list(reversed(map(int, l[i:i+4])))
            r.append(hex(ZZ(z,2)))

        r = sum([r[i:i+8]+[" "] for i in xrange(0,len(r),8) ],[])

        return "".join(r)[:-1]

    @classmethod
    def hexstringtobitstring(self, n, length=None):
        """
        The bit order is big endian.

        EXAMPLE::

            sage: KATAN32.hexstringtobitstring('1c', length=8)
            [0, 0, 0, 1, 1, 1, 0, 0]
        """
        if length is None:
            length = self.Bs
        n = int(n.replace(" ",""),16)
        l = []
        for i in xrange(length):
            l.append(1 if 2**i & n else 0)
        l = map(GF(2),l)
        return list(reversed(l))

    @classmethod
    def random_element(self, length=None):
        if length is None:
            return self.VS.random_element()
        else:
            return VectorSpace(GF(2),length).random_element()

    @classmethod
    def _scrub_input(self, I, length=None):
        """
        Returns a vector of length ``length''.

        INPUT:

        - ``I`` - list, string, vector
        - ``length`` - optional target length (default: blocksize)
        """
        VS = self.VS
        if I is None:
            return self.random_element(length=length)
        elif isinstance(I, basestring):
            return self.hexstringtobitstring(I)
        elif len(I) == self.Bs:
            try:
                return VS(I)
            except (TypeError, AttributeError):
                return FreeModule(parent(I[0]),self.Bs)(I)

        elif len(I) == 80:
            return VectorSpace(GF(2),80)(I)
        elif len(I) == 2 and len(I[0]) == self.Lcard[1] and len(I[1]) == self.Lcard[0]:
            return self.unslice(I)
        else:
            raise ValueError

    def varformatstr(self, name):
        l = str(len(str(self.nvars)))
        if not name.startswith("K"):
            name += self._postfix
        return name + "%0" + l + "d"

    def varstrs(self, name):
        s = self.varformatstr(name)
        if s.startswith("K"):
            return tuple([s%(i) for i in xrange(80)])
        else:
            return tuple([s%(i) for i in xrange(self.nvars)])

    def vars(self, name):
        gd = self.variable_dict()
        return tuple([gd[e] for e in self.varstrs(name)])

    def variable_dict(self):
        try:
            R,gd = self._variable_dict
            if R is self.R:
                return gd
            else:
                self._variable_dict = self.R, self.R.gens_dict()
            return gd
        except AttributeError:
            gd = self.R.gens_dict()
            self._variable_dict = self.R,gd
            return gd

    def ring(self, order=None, n=1, reverse_variables=True):
        if order is None:
            order = self._order
        if reverse_variables == False:
            f = lambda x: x
        else:
            f = lambda x: reversed(x)

        Bs = self.Bs
        nvars = self.nvars
        Ks = 80

        if n == 1:
            var_names = []
            if not reverse_variables:
                var_names += [self.varformatstr("K")%(j) for j in xrange(Ks)]
                var_names += [self.varformatstr("Y")%(j) for j in xrange(nvars)]
            else:
                var_names += [self.varformatstr("Y")%(j) for j in xrange(nvars)][::-1]
                var_names += [self.varformatstr("K")%(j) for j in xrange(Ks)]

        elif n > 1:
            var_names = []
            sample_length = len(str(n-1))

            if not reverse_variables:
                var_names += [self.varformatstr("K")%(j) for j in xrange(Ks)]
                for sample in xrange(n):
                    for j in xrange(nvars):
                        var_names.append(self.varformatstr(("Y%0"+ str(sample_length) + "d")%sample)%(j))
            else:
                for sample in xrange(n):
                    for j in range(nvars)[::-1]:
                        var_names.append(self.varformatstr(("Y%0"+ str(sample_length) + "d")%sample)%(j))
                var_names += [self.varformatstr("K")%(j) for j in xrange(Ks)]

        else:
            raise ValueError("parameter n must be >= 1.")

        R = BooleanPolynomialRing(len(var_names), var_names, order=order)
        if n == 1:
            self.R = R

        return R

    @classmethod
    def slice(self, I):
        """
        'The plaintext is loaded into two registers L1, and L2 (of
        respective lengths of 13 and 19 bits) where the least
        significant bit of the plaintext is loded to bit 0 of L2,
        while the most significant bit of the plaintext is loaded to
        bit 12 of L1.'
        """
        if isinstance(parent(I[0]), BooleanPolynomialRing_class):
            L1 = FreeModule(parent(I[0]),self.Lcard[0])(0)
            L2 = FreeModule(parent(I[0]),self.Lcard[1])(0)
        else:
            L1 = VectorSpace(GF(2),self.Lcard[0])(0)
            L2 = VectorSpace(GF(2),self.Lcard[1])(0)
        for i in range(len(L2)):
            L2[i] = I[i]
        for i in range(len(L1)):
            L1[i] = I[i+len(L2)]
        return L2,L1

    @classmethod
    def unslice(self, L):
        """
        'After 254 rounds of the cipher, the contents of the registers
        are then exported as the ciphertext (where bit 0 of L2 is the
        least significant of the ciphertext).'
        """
        L2, L1 = L
        if isinstance(parent(L2[0]), BooleanPolynomialRing_class):
            return FreeModule(parent(L2[0]),self.Bs)(list(L2)+list(L1))
        else:
            return VectorSpace(GF(2),self.Bs)(list(L2)+list(L1))

    @staticmethod
    def rot(v):
        """
        'Each round, L1 and L2 are shifted to the left (bit i is the
        shiften to position i+1)'

        NOTE::

            We start counting from the left and thus we shift to the
            *right*.
        """
        VS = parent(v)
        return VS(list(v[-1:])+list(v[:-1]))

    @staticmethod
    def rot_reverse(v):
        """
        NOTE::

            We start counting from the left and thus we shift to the
            *right*.
        """
        VS = parent(v)
        return VS(list(v[1:])+list(v[:1]))

    @classmethod
    def fa(self, L1, t, ka):
        x = self.x
        return L1[x[0]] + L1[x[1]] + (L1[x[2]] * L1[x[3]]) + (L1[x[4]] * t) + ka
    
    @classmethod
    def fb(self, L2, kb):
        y = self.y
        return L2[y[0]] + L2[y[1]] + (L2[y[2]] * L2[y[3]]) + (L2[y[4]] * L2[y[5]]) + kb

    @classmethod
    def key_schedule(self, K, IR):
        ka, kb = K[79], K[78]

        out = K[79] + K[60] + K[49] + K[12]
        K = KATAN.rot(K)
        K[0] = out

        out = K[79] + K[60] + K[49] + K[12]
        K = KATAN.rot(K)
        K[0] = out

        return K,IR, ka,kb
       
    @classmethod
    def round(self, L2, L1, t, ka, kb):
        ra = self.fa(L1, t, ka)
        rb = self.fb(L2, kb)
        L1 = KATAN.rot(L1)
        L2 = KATAN.rot(L2)
        L2[0] = ra
        L1[0] = rb
        return L2, L1

    def __call__(self, P, K, construct_solution=False):
        """
        Encrypt ``P`` using the key ``K``.

        EXAMPLES:

        We are testing against the official test vectors. Note that
        the bits in the official test vectors are reversed for some
        reason.::

            sage: k = KATAN32()
            sage: K = [1 for _ in range(80)]
            sage: P = [0 for _ in range(32)]
            sage: "".join(str(e) for e in reversed(k(P,K)))
            '01111110000111111111100101000101'

            sage: k = KATAN32()
            sage: K = [0 for _ in range(80)]
            sage: P = [1 for _ in range(32)]
            sage: "".join(str(e) for e in reversed(k(P,K)))
            '01000011001011100110000111011010'

            sage: k = KATAN48()
            sage: K = [1 for _ in range(80)]
            sage: P = [0 for _ in range(48)]
            sage: "".join(str(e) for e in reversed(k(P,K)))
            '010010110111111011111100111110111000011001011001'

            sage: k = KATAN48()
            sage: K = [0 for _ in range(80)]
            sage: P = [1 for _ in range(48)]
            sage: "".join(str(e) for e in reversed(k(P,K)))
            '101001001011110100011001011011010000101110000101'

            sage: k = KATAN64()
            sage: K = [1 for _ in range(80)]
            sage: P = [0 for _ in range(64)]
            sage: "".join(str(e) for e in reversed(k(P,K)))
            '0010000111110010111010011001110000001111101010111000001010001010'

            sage: k = KATAN64()
            sage: K = [0 for _ in range(80)]
            sage: P = [1 for _ in range(64)]
            sage: "".join(str(e) for e in reversed(k(P,K)))
            '1100100101010110000100000000110110111110101101100100101110101000'

            sage: k = KTANTAN32()
            sage: K = [1 for _ in range(80)]
            sage: P = [0 for _ in range(32)]
            sage: "".join(str(e) for e in reversed(k(P,K)))
            '00100010111010100011100110001000'

            sage: k = KTANTAN32()
            sage: K = [0 for _ in range(80)]
            sage: P = [1 for _ in range(32)]
            sage: "".join(str(e) for e in reversed(k(P,K)))
            '01000011001011100110000111011010'

            sage: k = KTANTAN32()
            sage: K = [1 for _ in range(80)]; K[0] = 0
            sage: P = [0 for _ in range(32)]
            sage: "".join(str(e) for e in reversed(k(P,K)))
            '01010010111001000000101011100001'

            sage: k = KTANTAN48()
            sage: K = [1 for _ in range(80)]
            sage: P = [0 for _ in range(48)]
            sage: "".join(str(e) for e in reversed(k(P,K)))
            '100100110110110100001111101000110011101000000101'

            sage: k = KTANTAN48()
            sage: K = [0 for _ in range(80)]
            sage: P = [1 for _ in range(48)]
            sage: "".join(str(e) for e in reversed(k(P,K)))
            '101001001011110100011001011011010000101110000101'

            sage: k = KTANTAN64()
            sage: K = [1 for _ in range(80)]
            sage: P = [0 for _ in range(64)]
            sage: "".join(str(e) for e in reversed(k(P,K)))
            '1100000000101101111000000101101111111010000110010100101100010110'

            sage: k = KTANTAN64()
            sage: K = [0 for _ in range(80)]
            sage: P = [1 for _ in range(64)]
            sage: "".join(str(e) for e in reversed(k(P,K)))
            '1100100101010110000100000000110110111110101101100100101110101000'
        """
        L2,L1 = self.slice(self._scrub_input(P))
        K = self._scrub_input(K)
        
        ir = IR()

        s = []

        for i in range(self.Nr):
            t = ir()
            K, ir, ka, kb = self.key_schedule(K, ir)
            L2,L1 = self.round(L2,L1, t, ka, kb)
            if construct_solution: s.append(L2[0]); s.append(L1[0])
            if self.Bs > 32:
                L2,L1 = self.round(L2,L1, t, ka, kb)
                if construct_solution: s.append(L2[0]); s.append(L1[0])
            if self.Bs > 48:
                L2,L1 = self.round(L2,L1, t, ka, kb)
                if construct_solution: s.append(L2[0]); s.append(L1[0])
        if construct_solution:
            s = dict(zip(self.vars("Y"), s))
            s.update(dict(zip(self.vars("K"), K)))
            return self.unslice((L2,L1)), s
        else:
            return self.unslice((L2,L1))

    def decrypt(self, C, K):
        """
        Decrypt ``C`` using the key ``K``.
        """
        assert(self.Bs == 32)
        L2,L1 = self.slice(self._scrub_input(C))
        K = self._scrub_input(K)
        
        ir = IR()

        ka,kb = [],[]

        x,y = self.x,self.y

        for i in range(self.Nr):
            t = ir()
            K, ir, _ka, _kb = self.key_schedule(K, ir)
            ka.append(_ka)
            kb.append(_kb)

        for i in range(self.Nr)[::-1]:
            fb = L1[0]
            fa = L2[0]
            L1 = KATAN.rot_reverse(L1)
            L2 = KATAN.rot_reverse(L2)
    
            L1[x[0]] = fa + L1[x[1]] + (L1[x[2]] * L1[x[3]]) + (L1[x[4]] * ir[i])    + ka[i]
            L2[y[0]] = fb + L2[y[1]] + (L2[y[2]] * L2[y[3]]) + (L2[y[4]] * L2[y[5]]) + kb[i]

        return self.unslice((L2,L1))


    @classmethod
    def round_polynomials(self, L2, L1, t, ka, kb, Y):
        ra = self.fa(L1, t, ka)
        rb = self.fb(L2, kb)
        L1 = KATAN.rot(L1)
        L2 = KATAN.rot(L2)
        L2[0] = Y.next()
        L1[0] = Y.next()
        return L2, L1, [L2[0] + ra, L1[0] + rb]

    def polynomial_system(self, P=None, K=None, full_solution=False, **kwds):
        """
        """
        KATAN = self.__class__

        L2,L1 = self.slice(self._scrub_input(P))
        L1 = FreeModule(self.R, self.Lcard[0])(L1)
        L2 = FreeModule(self.R, self.Lcard[1])(L2)
        working_key = self._scrub_input(K,80)

        Y = iter(self.vars("Y"))

        self.linear_approximations = []

        if "C" not in kwds:
            if full_solution:
                C, s = self((L2,L1), working_key, construct_solution=True)
            else:
                C = self((L2,L1), working_key)
                s = dict(zip(self.vars("K"), working_key))
        else:
            C = self._scrub_input(kwds["C"])
            s = dict(zip(self.vars("K"),working_key))
        C = self.slice(C)

        R = self.R

        polynomials = []

        K = FreeModule(self.R, 80)(self.vars("K"))

        ir = IR()

        for i in range(self.Nr):
            polynomials.append([])
            t = ir()
            K, ir, ka, kb = self.key_schedule(K, ir)
            if i > 70:
                x = self.x
                #lfa = L1[x[0]] + L1[x[1]] + (L1[x[4]] * t) + ka
                lfa = L1[x[2]]*L1[x[3]]
            L2, L1, polys = self.round_polynomials(L2, L1, t, ka, kb, Y)
            if i > 70:
                #lfa += L2[0]
                self.linear_approximations.append(lfa)
            polynomials[i].extend(polys)

            if self.Bs > 32:
                L2, L1, polys = self.round_polynomials(L2, L1, t, ka, kb, Y)
                polynomials[i].extend(polys)

            if self.Bs > 48:
                L2, L1, polys = self.round_polynomials(L2, L1, t, ka, kb, Y)
                polynomials[i].extend(polys)

        polynomials.append(map(sum,zip(C[0],L2)) + map(sum,zip(C[1],L1)))

        return MPolynomialSystem(R,polynomials), s

    def register_state(self, P, Nr=None):
        Nr = Nr if Nr is not None else self.Nr
        L2,L1 = self.slice(self._scrub_input(P))
        L1 = FreeModule(self.R, self.Lcard[0])(L1)
        L2 = FreeModule(self.R, self.Lcard[1])(L2)
        Y = iter(self.vars("Y"))

        for i in range(Nr):
            if self.Bs in (32,48,64):
                L1 = KATAN.rot(L1)
                L2 = KATAN.rot(L2)
                L2[0] = Y.next()
                L1[0] = Y.next()
            if self.Bs in (48,64):
                L1 = KATAN.rot(L1)
                L2 = KATAN.rot(L2)
                L2[0] = Y.next()
                L1[0] = Y.next()
            if self.Bs in (64,):
                L1 = KATAN.rot(L1)
                L2 = KATAN.rot(L2)
                L2[0] = Y.next()
                L1[0] = Y.next()
        return L2,L1

class KTANTAN(KATAN):
    @staticmethod
    def key_schedule(K, IR):
        one, zero = GF(2)(1),GF(2)(0)
        flip = lambda x: zero if x else one
        MUX = lambda x,y: x[y]

        T = list(IR.T)

        W0,W1,W2,W3,W4 = K[:16],K[16:32],K[32:48],K[48:64],K[64:]
        T7T6T5T4 = 8*int(T[7]) + 4*int(T[6]) + 2*int(T[5]) + int(T[4])
        w0 = MUX(W0,T7T6T5T4)
        w1 = MUX(W1,T7T6T5T4)
        w2 = MUX(W2,T7T6T5T4)
        w3 = MUX(W3,T7T6T5T4)
        w4 = MUX(W4,T7T6T5T4)

        T3orT2    = one if (T[3] or       T[2]) else zero
        T3ornotT2 = one if (T[3] or flip(T[2])) else zero

        T1T0    = 2*int(      T[1])+ int(      T[0])
        notT1T0 = 2*int(flip(T[1]))+ int(flip(T[0]))

        ka = flip(T[3])*flip(T[2])*w0 +   ( T3orT2)*MUX([w1,w2,w3,w4],T1T0)
        kb = flip(T[3])*     T[2] *w4 + (T3ornotT2)*MUX([w0,w1,w2,w3],notT1T0)

        return K, IR, ka, kb

    def __repr__(self):
        """
        EXAMPLE::

            sage: KATAN32(Nr=10) # indirect doctest
            KATAN32-10
        """
        return "KTANTAN%d-%d"%(self.__class__.Bs,self.Nr)    

class KATAN32(KATAN):
    Bs = 32
    VS = VectorSpace(GF(2),Bs)
    Lcard = (13,19)
    x = (12,7,8,5,3)
    y = (18,7,12,10,8,3)

class KATAN48(KATAN):
    Bs = 48
    VS = VectorSpace(GF(2),Bs)
    Lcard = (19,29)
    x = (18,12,15,7,6)
    y = (28,19,21,13,15,6)

class KATAN64(KATAN):
    Bs = 64
    VS = VectorSpace(GF(2),Bs)
    Lcard = (25,39)
    x = (24,15,20,11,9)
    y = (38,25,33,21,14,9)

class KTANTAN32(KTANTAN,KATAN32):
    pass

class KTANTAN48(KTANTAN,KATAN48):
    pass

class KTANTAN64(KTANTAN,KATAN64):
    pass

class IR(SageObject):
    """
    Round constant factory.
    """
    _IR = [1,1,1,1,1,1,1,0,0,0, 1,1,0,1,0,1,0,1,0,1, 1,1,1,0,1,1,0,0,1,1, 
           0,0,1,0,1,0,0,1,0,0, 0,1,0,0,0,1,1,0,0,0, 1,1,1,1,0,0,0,0,1,0,
           0,0,0,1,0,1,0,0,0,0, 0,1,1,1,1,1,0,0,1,1, 1,1,1,1,0,1,0,1,0,0, 
           0,1,0,1,0,1,0,0,1,1, 0,0,0,0,1,1,0,0,1,1, 1,0,1,1,1,1,1,0,1,1,
           1,0,1,0,0,1,0,1,0,1, 1,0,1,0,0,1,1,1,0,0, 1,1,0,1,1,0,0,0,1,0, 
           1,1,1,0,1,1,0,1,1,1, 1,0,0,1,0,1,1,0,1,1, 0,1,0,1,1,1,0,0,1,0,
           0,1,0,0,1,1,0,1,0,0, 0,1,1,1,0,0,0,1,0,0, 1,1,1,1,0,1,0,0,0,0, 
           1,1,1,0,1,0,1,1,0,0, 0,0,0,1,0,1,1,0,0,1, 0,0,0,0,0,0,1,1,0,1,
           1,1,0,0,0,0,0,0,0,1, 0,0,1,0]

    @classmethod
    def __getitem__(self, i):
        """
        Return the ``i``-th round constant.

        INPUT:

        - ``i`` - an integer > 0 and < 254.

        EXAMPLE::

            sage: IR()[0]
            1
        """
        return self._IR[i]

    @staticmethod
    def verify():
        """
        Verify that the dynamic generation of round constants matches
        the constants from the paper.


        EXAMPLE::
        
            sage: IR.verify()
        """
        ir = IR()
        for i in range(254):
            assert(ir[i] == ir())

    def __init__(self):
        self.T = VectorSpace(GF(2),8)([1,1,1,1, 1,1,1,1])

    def __call__(self):
        T = self.T
        r = T[7] + T[6] + T[4] + T[2]
        self.T = KATAN.rot(T)
        self.T[0] = r
        return self.T[7]

    @classmethod
    def __getitem__(self, i):
        return self._IR[i]

    @staticmethod
    def verify():
        ir = IR()
        for i in range(254):
            assert(ir[i] == ir())

def katan_dc_right_pair(self, **kwds):
    """
    """
    KATAN = self.__class__
    Bs = self.Bs

    self = copy(self)
    r = kwds.get("r", 1)
    K = kwds.get("K", self.random_element(80))
    kwds["K"] = K

    R = self.ring(n=2)
    variable_names = ["P0000%03d"%i for i in range(Bs)] + ["P1000%03d"%i for i in range(Bs)] \
        + list(R.variable_names()) + \
        ["C0000%03d"%i for i in range(Bs)] + ["C1000%03d"%i for i in range(Bs)]
    term_order = R.term_order()
                
    R = BooleanPolynomialRing(len(variable_names), variable_names, order=term_order)
    self.R = R
    P0 = [R("P0000%03d"%i) for i in range(Bs)]
    P1 = [R("P1000%03d"%i) for i in range(Bs)]
    C0 = [R("C0000%03d"%i) for i in range(Bs)]
    C1 = [R("C1000%03d"%i) for i in range(Bs)]
    # request new plaintexts
    DP = KATAN.str_to_dc("1101010100010000000 0001000000000")
    P1 = [P0[i] + DP[i] for i in range(Bs)]

    l = []

    # generate polynomial systems
    gen0 = self.new_generator(postfix='0')
    gen0.R = R
    F,s = gen0.polynomial_system(P=P0, C=C0, **kwds)
    for i in xrange(r):
        l.append(F.round(i))

    gen1 = self.new_generator(postfix='1')
    gen1.R = R
    F,s = gen1.polynomial_system(P=P1, C=C1, **kwds)
    for i in xrange(r):
        l.append(F.round(i))

    r2 = 0
    for r2 in range(1,r+1,16):
        Y0 = gen0.unslice(gen0.register_state(P=P0,Nr=r2))
        Y1 = gen1.unslice(gen1.register_state(P=P1,Nr=r2))
        p, L2, L1 = propagate_differential32(DP,0,r2,recurse=False,limit=None,quite=True)
        DY = L2.list() + L1.list()
        
        diff = []
        for i in range(self.Bs):
            diff.append(Y0[i] + Y1[i] + DY[i])
        l.append(diff)

    if r2 != r:
        Y0 = gen0.unslice(gen0.register_state(P=P0,Nr=r))
        Y1 = gen1.unslice(gen1.register_state(P=P1,Nr=r))
        p, L2, L1 = propagate_differential32(DP,0,r,recurse=False,limit=None,quite=True)
        DY = L2.list() + L1.list()
    
        diff = []
        for i in range(self.Bs):
            diff.append(Y0[i] + Y1[i] + DY[i])
        l.append(diff)

    if "bit" in kwds:
        l.append([P0[0] + kwds["bit"]])

    l.append(map(add,zip(self.vars("K"),K)))

    F =  MPolynomialSystem(R,l)
    F.ring().set()
    s,t = minisat2(F)
    print t

    return [s[p] for p in P0], K


def katan_dc_implications(self, **kwds):
    """
    """
    KATAN = self.__class__
    Bs = self.Bs

    self = copy(self)
    r = kwds.get("r", 1)
    K = kwds.get("K", self.random_element(80))
    kwds["K"] = K

    R = self.ring(n=2, reverse_variables=True)
    variable_names =  ["C0000%03d"%i for i in range(Bs)] + ["C1000%03d"%i for i in range(Bs)] + \
        list(R.variable_names()) + \
        ["P0000%03d"%i for i in range(Bs)] + ["P1000%03d"%i for i in range(Bs)]
    term_order = "deglex(%d),deglex(%d)"%(len(variable_names)-2*Bs-80,2*Bs+80)
                
    R = BooleanPolynomialRing(len(variable_names), variable_names, order=term_order)
    self.R = R
    P0 = [R("P0000%03d"%i) for i in range(Bs)]
    P1 = [R("P1000%03d"%i) for i in range(Bs)]
    C0 = [R("C0000%03d"%i) for i in range(Bs)]
    C1 = [R("C1000%03d"%i) for i in range(Bs)]
    # request new plaintexts
    DP = KATAN.str_to_dc("1101010100010000000 0001000000000")
    P1 = [P0[i] + DP[i] for i in range(Bs)]

    l = []

    # generate polynomial systems
    gen0 = self.new_generator(postfix='0')
    gen0.R = R
    F,s = gen0.polynomial_system(P=P0, C=C0, **kwds)
    for i in xrange(r):
        l.append(F.round(i))

    gen1 = self.new_generator(postfix='1')
    gen1.R = R
    F,s = gen1.polynomial_system(P=P1, C=C1, **kwds)
    for i in xrange(r):
        l.append(F.round(i))

    r2 = 0
    for r2 in range(1,r+1,16):
        Y0 = gen0.unslice(gen0.register_state(P=P0,Nr=r2))
        Y1 = gen1.unslice(gen1.register_state(P=P1,Nr=r2))
        p, L2, L1 = propagate_differential32(DP,0,r2,recurse=False,limit=None,quite=True)
        DY = L2.list() + L1.list()
        
        diff = []
        for i in range(self.Bs):
            diff.append(Y0[i] + Y1[i] + DY[i])
        l.append(diff)

    if r2 != r:
        Y0 = gen0.unslice(gen0.register_state(P=P0,Nr=r))
        Y1 = gen1.unslice(gen1.register_state(P=P1,Nr=r))
        p, L2, L1 = propagate_differential32(DP,0,r,recurse=False,limit=None,quite=True)
        DY = L2.list() + L1.list()
    
        diff = []
        for i in range(self.Bs):
            diff.append(Y0[i] + Y1[i] + DY[i])
        l.append(diff)

    F =  MPolynomialSystem(R,l)
    return F

def katan_dc_factory(self, **kwds):
    """
                D L2             D L1     IR
     0: 1101010100010000000 0001000000000  1
     1: 1010101000100000000 0010000000000  1
        ...  
    42: 0000000100001000000 0000000001000
    """
    Bs = self.Bs
    KATAN = self.__class__

    self = copy(self)
    r = kwds.get("r", 0)

    control = kwds.get("control",False)

    #assert(r in (0,42))

    K = kwds.get("K", self.random_element(length=80))
    characteristic = kwds.get("characteristic", True)
    kwds["K"] = K

    last_round = kwds.get("last_round", True)
    leave_out_last_round = False
    symbolic_last_round = False

    if last_round == False:
        leave_out_last_round = True
    elif last_round == 'symbolic':
        symbolic_last_round = True

    if not control:
        R = self.ring(n=2)
    else:
        R = self.ring(n=3)
    self.R = R

    s = {}

    if symbolic_last_round:
        variable_names = list(R.variable_names()) + ["C0000%03d"%i for i in range(Bs)] + ["C1000%03d"%i for i in range(Bs)]
        term_order = R.term_order()
        if "," in term_order.name():
            term_order = term_order.name() + ",deglex(%d)"%(2*Bs)
        else:
            term_order = term_order.name() + "(%d)"%(len(variable_names)-2*Bs)+ ","+term_order.name()+"(%d)"%(2*Bs)
                
        R = BooleanPolynomialRing(len(variable_names), variable_names, order=term_order)
        Ca = [R("C0000%03d"%i) for i in range(Bs)]
        Cb = [R("C1000%03d"%i) for i in range(Bs)]
    # request new plaintexts
    #diff_iter = AuthorsDiffIter()
    P0 = kwds.get("P0", self.random_element())

    DP = KATAN.str_to_dc("1101010100010000000 0001000000000")
    if kwds.get('randomize', False):
        P1 = self.random_element()
    else:
        P1 = [P0[i] + DP[i] for i in range(self.Bs)]

    l = []

    # generate polynomial systems
    gen0 = self.new_generator(postfix='0')
    gen0.R = R
    C0 = gen0(P0, K)
    F,s0 = gen0.polynomial_system(P=P0, full_solution=True, **kwds)
    linear_approximations = list(gen0.linear_approximations)
    s.update(s0)
    #Y0 = gen0.vars("Y")
    for i in xrange(F.nrounds()):
        if characteristic or i >= r:
            l.append(F.round(i))

    if symbolic_last_round:
        lr = l[-1]
        lr = [lr[i] - C0[i] + Ca[i] for i in range(self.Bs)]
        l = l[:-1] + [lr]
    elif leave_out_last_round:
        l = l[:-1]
    if "C0" in kwds:
        lr = l[-1]
        lr = [lr[i] - C0[i] + kwds["C0"][i] for i in range(self.Bs)]
        l = l[:-1] + [lr]


    gen1 = self.new_generator(postfix='1')
    gen1.R = R
    C1 = gen1(P1, K)
    F,s1 = gen1.polynomial_system(P=P1, full_solution=True, **kwds)
    linear_approximations.extend(list(gen1.linear_approximations))
    s.update(s1)
    #Y1 = gen1.vars("Y")
    for i in xrange(F.nrounds()):
        if characteristic or i >= r:
            l.append(F.round(i))

    if symbolic_last_round:
        lr = l[-1]
        lr = [lr[i] - C1[i] + Cb[i] for i in range(self.Bs)]
        l = l[:-1] + [lr]
    elif leave_out_last_round:
        l = l[:-1]
    elif "C1" in kwds:
        lr = l[-1]
        lr = [lr[i] - C1[i] + kwds["C1"][i] for i in range(self.Bs)]
        l = l[:-1] + [lr]

    gd = R.gens_dict()

    r2 = 0
    for r2 in range(1,r+1,16):
        Y0 = gen0.unslice(gen0.register_state(P=P0,Nr=r2))
        Y1 = gen1.unslice(gen1.register_state(P=P1,Nr=r2))
        p, L2, L1 = propagate_differential32(DP,0,r2,recurse=False,limit=None,quite=True)
        DY = L2.list() + L1.list()
        
        diff = []
        for i in range(self.Bs):
            diff.append(Y0[i] + Y1[i] + DY[i])
        l.append(diff)

    if r2 != r:
        Y0 = gen0.unslice(gen0.register_state(P=P0,Nr=r))
        Y1 = gen1.unslice(gen1.register_state(P=P1,Nr=r))
        p, L2, L1 = propagate_differential32(DP,0,r,recurse=False,limit=None,quite=True)
        DY = L2.list() + L1.list()
    
        diff = []
        for i in range(self.Bs):
            diff.append(Y0[i] + Y1[i] + DY[i])
        l.append(diff)
    #print "Probability: 2^-%d"%log_b(p,2)

    if control:

        D2 = KATAN.str_to_dc("0000000000000000000 0000000000001")
        P2 = [P0[i] + D2[i] for i in range(self.Bs)]
        gen2 = self.new_generator(postfix='2')
        gen2.R = R
        C2 = gen2(P2, K)
        F,s2 = gen2.polynomial_system(P=P2, full_solution=True, **kwds)
        s.update(s2)
        for i in xrange(F.nrounds()):
            if characteristic or i >= r:
                l.append(F.round(i))

    shuffle(linear_approximations)
    la = [linear_approximations[i] for i in range(kwds.get("n_lin",0))]
    if la:
        l.append(la)
        
    F =  MPolynomialSystem(R,l)
    return F,s

def katan_ia_factory(self, n=2, **kwds):
    """
    """
    Bs = self.Bs
    KATAN = self.__class__

    self = copy(self)
    K = kwds.get("K", self.random_element(length=80))
    kwds["K"] = K
    R = self.ring(n=2**n, reverse_variables=True)
    self.R = R

    l = []

    s = {}

    P = self.random_element()
    for i,v in enumerate(VectorSpace(GF(2),n)):
        DP = v.list() + [0 for _ in range(Bs - n)]
        Pi = [P[j] + DP[j] for j in range(Bs)]
        gen = self.new_generator(postfix=("%0" + str(len(str(2**n-1))) + "d")%i)
        gen.R = R
        F, si = gen.polynomial_system(P=Pi, full_solution=True, **kwds)
        s.update(si)
        l.extend(F.rounds())

    F =  MPolynomialSystem(R,l)
    return F,s


def minisat_experiment(p, r, trials, **kwds):
    T = []
    passes = 0
    timeout = kwds.get("timeout",0)
    timedout = 0
    factory = kwds.get("factory",katan_dc_factory)
    for i in range(trials): 
        wt = walltime()
        F,s = factory(p, r=r, **kwds)
        F = F.eliminate_linear_variables(1)
        try:
            #a2 = ANFSatSolver(F.ring())
            alarm(timeout) # arm alarm
            #s,t = a2(F)
            s,t = minisat2(F)
            alarm(0)
            if s:
                passes += 1
            T.append(t)
            print "i: %5d   passes: %3d   timeout: %3d   min:   %.3f   avg: %.3f   med: %.3f   max: %.3f"%(i+1,passes,timedout,min(T),sum(T)/len(T),sorted(T)[len(T)/2],max(T))
        except KeyboardInterrupt:
            print "i: %5d   TIMEOUT"%(i+1)
            timedout += 1
        sys.stdout.flush()
    print
    print "min: %9.3f\navg: %9.3f\nmed: %9.3f\nmax: %9.3f"%(min(T),sum(T)/len(T),sorted(T)[len(T)/2],max(T))


def polybori_experiment(p, r, trials, **kwds):
    T = []
    passes = 0
    factory = kwds.get("factory",katan_boomerang_factory)
    if factory is katan_boomerang_factory:
        print katan_boomerang_factory(p, r=r, prob_only=True)

    for i in range(trials): 
        wt = walltime()
        F,s = factory(p, r=r, **kwds)
        try:
            t = cputime()
            F = F.eliminate_linear_variables(10**4)
            gb = F.groebner_basis(faugere=False,linear_algebra_in_last_block=False,heuristic=False)
            t = cputime(t)
            wt = walltime(wt)
            if gb != [1]:
                passes += 1
            T.append(t)
            
            print "i: %5d   passes: %3d   wall: %.3f   min:   %.3f   avg: %.3f   med: %.3f   max: %.3f"%(i+1,passes,wt,min(T),sum(T)/len(T),sorted(T)[len(T)/2],max(T))
        except KeyboardInterrupt:
            print "i: %5d   TIMEOUT"%(i+1)
        sys.stdout.flush()
    print
    print "min: %9.3f\navg: %9.3f\nmed: %9.3f\nmax: %9.3f"%(min(T),sum(T)/len(T),sorted(T)[len(T)/2],max(T))
    return sum(T)/len(T)

    
@cached_function
def propagate_differential32(DP,start_r,stop_r, prob=1, recurse=False, limit=None, quite=False):
    if limit is not None and recurse and prob >= limit:
        recurse = False
    # first we compute the difference distribution matrices
    Dfa = Matrix(ZZ,2,2**4)
    DfA = Matrix(ZZ,2,2**5)
    Dfb = Matrix(ZZ,2,2**6)
    B,(x0,x1,x2,x3) = BooleanPolynomialRing('x0,x1,x2,x3').objgens()
    fa = x0 + x1 + x2*x3
    B,(x0,x1,x2,x3,x4) = BooleanPolynomialRing('x0,x1,x2,x3,x4').objgens()
    fA = x0 + x1 + x2*x3 + x4
    B,(x0,x1,x2,x3,x4,x5) = BooleanPolynomialRing('x0,x1,x2,x3,x4,x5').objgens()
    fb = x0 + x1 + x2*x3 + x4*x5

    for v in VectorSpace(GF(2),4):
        for i,w in enumerate(VectorSpace(GF(2),4)):
            Dfa[int(fa(*v) + fa(*(v+w))),i] += 1

    for v in VectorSpace(GF(2),5):
        for i,w in enumerate(VectorSpace(GF(2),5)):
            DfA[int(fA(*v) + fA(*(v+w))),i] += 1

    for v in VectorSpace(GF(2),6):
        for i,w in enumerate(VectorSpace(GF(2),6)):
            Dfb[int(fb(*v) + fb(*(v+w))),i] += 1

    ktn = KTANTAN32(stop_r)
    ir = IR()
    L2,L1 = ktn.slice(ktn._scrub_input(DP))

    x = ktn.x
    y = ktn.y

    def to_int(*l):
        return sum(int(l[i])*2**(i) for i in range(len(l)))

    if not quite:
        print "INP %3d"%(int(log_b(prob,2))), "".join(str(e) for e in L2), "".join(str(e) for e in L1)

    for i in range(start_r,stop_r):
        t = ir[i]
        
        if not t:
            X = to_int(L1[x[0]],L1[x[1]],L1[x[2]],L1[x[3]])
            if Dfa[0,X] > Dfa[1,X]:
                ra = 0
            elif Dfa[0,X] < Dfa[1,X]:
                ra = 1
            elif recurse:
                prob *= 2
                ra = None
            else:
                prob *= 2
                ra = 0
        else:
            X = to_int(L1[x[0]],L1[x[1]],L1[x[2]],L1[x[3]],L1[x[4]])
            if DfA[0,X] > DfA[1,X]:
                ra = 0
            elif DfA[0,X] < DfA[1,X]:
                ra = 1
            elif recurse:
                prob *= 2
                ra = None # we are dealing with this later
            else:
                prob *= 2
                ra = 0 # we make a choice (biased towards low weigth)

        Y = to_int(L2[y[0]],L2[y[1]],L2[y[2]],L2[y[3]],L2[y[4]],L2[y[5]])
        if Dfb[0,Y] > Dfb[1,Y]:
            rb = 0
        elif Dfb[0,Y] < Dfb[1,Y]:
            rb = 1
        elif recurse:
            prob *= 2
            rb = None
        else:
            prob *= 2
            rb = 0
    
        L1 = KATAN.rot(L1)
        L2 = KATAN.rot(L2)
  
        if ra is None:
            if rb is None:
                L2l,L1l = [copy(L2) for _ in range(4)], [copy(L1) for _ in range(4)]
                L2l[0][0] = 0
                L2l[1][0] = 0
                L2l[2][0] = 1
                L2l[3][0] = 1

                L1l[0][0] = 0
                L1l[1][1] = 1
                L1l[2][0] = 0
                L1l[3][1] = 1

                r = [propagate_differential32((L2l[0],L1l[0]),i+1,stop_r,prob,recurse,limit,quite),
                     propagate_differential32((L2l[1],L1l[1]),i+1,stop_r,prob,recurse,limit,quite),
                     propagate_differential32((L2l[2],L1l[2]),i+1,stop_r,prob,recurse,limit,quite),
                     propagate_differential32((L2l[3],L1l[3]),i+1,stop_r,prob,recurse,limit,quite)]

                r = sorted(r,key=lambda x: x[0])
                return r[0]

            else:
                L2l,L1l = [copy(L2) for _ in range(2)], [copy(L1) for _ in range(2)]
                L2l[0][0] = 0
                L2l[1][0] = 1

                L1l[0][0] = rb
                L1l[1][1] = rb

                r = [propagate_differential32((L2l[0],L1l[0]),i+1,stop_r,prob,recurse,limit,quite),
                     propagate_differential32((L2l[1],L1l[1]),i+1,stop_r,prob,recurse,limit,quite)]

                r = sorted(r,key=lambda x: x[0])
                return r[0]

        elif rb is None:
            L2l,L1l = [copy(L2) for _ in range(2)], [copy(L1) for _ in range(2)]
            L2l[0][0] = ra
            L2l[1][0] = ra

            L1l[0][0] = 0
            L1l[1][1] = 1

            r = [propagate_differential32((L2l[0],L1l[0]),i+1,stop_r,prob,recurse,limit,quite),
                 propagate_differential32((L2l[1],L1l[1]),i+1,stop_r,prob,recurse,limit,quite)]

            r = sorted(r,key=lambda x: x[0])
            return r[0]

        L2[0] = ra
        L1[0] = rb
        
        if not quite:
            print "%3d %3d"%(i,int(log_b(prob,2))), "".join(str(e) for e in L2), "".join(str(e) for e in L1)
        
    return prob,L2,L1

@cached_function
def propagate_differential32_reverse(DP,start_r,stop_r, prob=1, quite=False):
    Dfa = Matrix(ZZ,2,2**4)
    DfA = Matrix(ZZ,2,2**5)
    Dfb = Matrix(ZZ,2,2**6)
    B,(x0,x1,x2,x3) = BooleanPolynomialRing('x0,x1,x2,x3').objgens()
    fa = x0 + x1 + x2*x3
    B,(x0,x1,x2,x3,x4) = BooleanPolynomialRing('x0,x1,x2,x3,x4').objgens()
    fA = x0 + x1 + x2*x3 + x4
    B,(x0,x1,x2,x3,x4,x5) = BooleanPolynomialRing('x0,x1,x2,x3,x4,x5').objgens()
    fb = x0 + x1 + x2*x3 + x4*x5

    for v in VectorSpace(GF(2),4):
        for i,w in enumerate(VectorSpace(GF(2),4)):
            Dfa[int(fa(*v) + fa(*(v+w))),i] += 1

    for v in VectorSpace(GF(2),5):
        for i,w in enumerate(VectorSpace(GF(2),5)):
            DfA[int(fA(*v) + fA(*(v+w))),i] += 1

    for v in VectorSpace(GF(2),6):
        for i,w in enumerate(VectorSpace(GF(2),6)):
            Dfb[int(fb(*v) + fb(*(v+w))),i] += 1

    ktn = KTANTAN32(stop_r)
    ir = IR()
    L2,L1 = ktn.slice(ktn._scrub_input(DP))

    x = ktn.x
    y = ktn.y

    def to_int(*l):
        return sum(int(l[i])*2**(i) for i in range(len(l)))

    if not quite:
        print "INP %3d"%(int(log_b(prob,2))), "".join(str(e) for e in L2), "".join(str(e) for e in L1)

    for i in range(start_r,stop_r)[::-1]:
        t = ir[i]
        
        fb = L1[0]
        fa = L2[0]

        L1 = KATAN.rot_reverse(L1)
        L2 = KATAN.rot_reverse(L2)

        if not t:
            X = to_int(fa,L1[x[1]],L1[x[2]],L1[x[3]])
            if Dfa[0,X] > Dfa[1,X]:
                ra = 0
            elif Dfa[0,X] < Dfa[1,X]:
                ra = 1
            else:
                prob *= 2
                ra = 0
        else:
            X = to_int(fa,L1[x[1]],L1[x[2]],L1[x[3]],L1[x[4]])
            if DfA[0,X] > DfA[1,X]:
                ra = 0
            elif DfA[0,X] < DfA[1,X]:
                ra = 1
            else:
                prob *= 2
                ra = 0 # we make a choice (biased towards low weigth)

        Y = to_int(fb,L2[y[1]],L2[y[2]],L2[y[3]],L2[y[4]],L2[y[5]])
        if Dfb[0,Y] > Dfb[1,Y]:
            rb = 0
        elif Dfb[0,Y] < Dfb[1,Y]:
            rb = 1
        else:
            prob *= 2
            rb = 0
    
  
        L1[x[0]] = ra
        L2[y[0]] = rb
        
        if not quite:
            print "%3d %3d"%(i,int(log_b(prob,2))), "".join(str(e) for e in L2), "".join(str(e) for e in L1)
        
    return prob,L2,L1

            
