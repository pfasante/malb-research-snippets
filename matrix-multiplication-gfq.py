c_david      = [0,1,3,6, 9,13,15,22, 24,30,33,46, 42,60,51,75, 60,94, 69]
c_montgomery = [0,1,3,6, 9,13,17,22, 27,34,39,46, 51,60,66,75, 81,94,102]
c_mip        = [0,1,3,6, 9,13,17,22, 27,31,36,40, 45,49,55,60, 66,71, 75]


def count_irred(n,q=2):
    n = ZZ(n)
    return 1/n * sum(moebius(n/d)*q**d for d in n.divisors())


def matmul_complexity_simple(n, c=None, start_at=1):
    if c is None:
        c = c_montgomery

    c = c + [infinity for _ in range(n-len(c)+1)]
    for length in range(start_at,n+1):
        need_md = 2*(length-1) # we get need_md + 1 by evaluating at infinity
        have_md = 0
        irred_d = 1
        irred = {}

        while have_md < need_md:
            irred[irred_d] = count_irred(irred_d)
            if have_md + irred_d * irred[irred_d] < need_md:
                have_md += irred_d * irred[irred_d]
            else:
                delta_md = need_md - have_md
                irred[irred_d] = ceil(delta_md/float(irred_d))
                have_md += irred_d * irred[irred_d]
            irred_d += 1

        delta_md = have_md - need_md
        if delta_md and irred[delta_md] > 0:
            irred[delta_md] -= 1
            have_md -= delta_md

        C = 1
        l = ["1"]
        for deg,count in irred.iteritems():
            C += count*c[deg]
            if count and get_verbose() >= 1:
                l.append( "%d*%d"%(count,deg) )
        if get_verbose() >= 1:
            print "%10s, cost: %4d, moduli prod deg: %4d,"%("GF(2^%d)"%length, C,need_md+1),
            print " = ",
            print " + ".join(l)

        if C < c[length]:
            c[length] = C
    return c

def mip(deg, c=None, q=2, multiplier=1):
    if c is None:
        c = c_david

    M = MixedIntegerLinearProgram(maximization=False)
    x = M.new_variable(integer=True)
    nvars = ceil(log(2*deg,2.0))

    M.set_objective(sum(c[i+1]*x[i] for i in range(nvars)))

    for i in range(nvars):
        M.add_constraint(x[i], min=0)

    limits = [0 for _ in range(nvars)]
    divisors  = [[] for _ in range(nvars)]
    for i in range(nvars):
        if is_even(i+1):
            divisors[i] = [i] + divisors[(i+1)/2-1]
            limits[i] = limits[(i+1)/2-1] + count_irred(i+1,q=q)
        else:
            divisors[i] = [i]
            limits[i] = count_irred(i+1,q=q)
        M.add_constraint(sum(x[j] for j in divisors[i]),max=limits[i],min=0)

    M.add_constraint(1 + sum( (i+1)*x[i] for i in range(nvars) ), min=2*deg-1)

    cost = multiplier * (ZZ(M.solve()) + 1)
    if get_verbose() >= 1:
        l = ["1"]
        for i in range(nvars):
            if M.get_values(x[i]):
                l.append("%d*%d"%(M.get_values(x[i]),i+1))

        print "%10s, cost: %4d, moduli prod deg: %4d,"%("GF(%d^%d)"%(q,deg), cost, 2*deg-1),
        print " = ",
        print " + ".join(l)
    return cost


def matmul_complexity_gf(deg, c=None, q=2):
    """
    INPUT:

    - ``deg`` - the degree of the extension field
    - ``c``   - a cost table mapping degrees to costs (default: ``c_david``)
    - ``q``   - the characteristic of the field

    OUTPUT:

        cost of multiplying in GF(q^deg) as number of multiplications mod q. If get_verbose() >= 1
        additional information is printed.
    """
    if c is None:
        c = c_david
    cost = mip(deg=deg, c=c, q=q)

    if (deg%2 == 0):
        c1 = mip(deg=deg/2, c=c, q=q**2, multiplier=3)
        if c1 < cost:
            cost = c1
    if (deg%3 == 0):
        c1 = mip(deg=deg/3, c=c, q=q**3, multiplier=6)
        if c1 < cost:
            cost = c1
    if (deg%5 == 0):
        c1 = mip(deg=deg/5, c=c, q=q**5, multiplier=13)
        if c1 < cost:
            cost = c1
    if get_verbose() >= 1:
        print
    return cost

