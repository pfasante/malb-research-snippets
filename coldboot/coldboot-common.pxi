cdef extern from "coldboot-common.h":
    unsigned int rol32_c "rol32"(unsigned int word, unsigned int shift)
    unsigned int ror32_c "ror32"(unsigned int word, unsigned int shift)

    int hw(unsigned int word)

    unsigned int randomize_word(unsigned int, double prob, int xor)

cdef extern from "arpa/inet.h":
    cdef unsigned int htonl(unsigned int hostlong)
    cdef unsigned int ntohl(unsigned int hostlong)






