#!/usr/bin/env sage
# -*- coding: utf-8 -*-

# Original code written by Martin Albrecht.  Minor compatibility changed by David Montminy. 
# The coldboot experiment functions where extracted from a previous version of anf2mip.py written by Martin Albrecht. 
# This code requires modified version of anf2mip.py for Sage 4.7.1 + #10879 (optional package)

import os
import sys
from optparse import OptionParser

from sage.all import *

def main():
    parser = OptionParser(usage="""\
Runs Coldboot experiments.

Usage: %prog [options]
""")
    parser.add_option('-c', '--cipher',
                      type='string', action='store',
                      help="""Either 'serpent' or 'aes'.""")
    parser.add_option('-n', '--num', 
                      type='int', action='store', default=100, 
                      help="""Number of experiments""")
    parser.add_option('-s', '--size', 
                      type='int', action='store', 
                      help="""Problem size.""")
    parser.add_option('-o', '--offset',
                      type='int', action='store', default=0,
                      help="""Offset.""")

    parser.add_option('--delta0', 
                      type='float', action='store', default=0.05,
                      help="""delta_0.""")
    parser.add_option('--delta1', 
                      type='float', action='store', default=0.001,
                      help="""delta_1.""")

    parser.add_option('-m', '--mode', 
                      type='string', action='store', default='aggressive',
                      help="""Either 'aggressive' or 'coldboot'.""")

    parser.add_option('-t', '--timeout', 
                      type='float', action='store', default=0.0,
                      help="""Timeout in seconds'.""")

    parser.add_option('-v', '--verbosity',
                      type='int', action='store', default=0,
                      help="""Verbosity level.""")

    parser.add_option('-p', '--parameters',
                      type='string', action='store', default='scip-settings-coldboot-default.set',
                      help="""SCIP parameters file.""")

    opts, args = parser.parse_args()
    if opts.cipher is None or opts.size is None:
        parser.print_help()
        return

    if opts.cipher == 'serpent':
        from serpent_coldboot import test_serpent_mip
#    elif opts.cipher == 'aes':
#        from anf2mip import test_aes_mip

    if opts.mode == "coldboot":
        opts.mode = True

    if opts.parameters == "default":
        opts.parameters = None

    T = []
    C = []
    O = []
    set_verbose(opts.verbosity)

    for i in range(opts.num):
        set_random_seed(i)
        if opts.cipher == 'serpent':
            t, obj, correct = test_serpent_mip(opts.size,
                                               delta0=opts.delta0,
                                               delta1=opts.delta1,
                                               coldboot=opts.mode,
                                               solver='SCIP2',
                                               timeout=opts.timeout,
                                               parameters=opts.parameters,
                                               shift=opts.offset)
        if opts.cipher == 'aes':
            t, obj, correct = test_aes_mip(opts.size, 4, 4, 8,
                                           delta0=opts.delta0,
                                           delta1=opts.delta1,
                                           coldboot=opts.mode,
                                           solver='SCIP2',
                                           timeout=opts.timeout,
                                           parameters=opts.parameters)
        T.append(t)
        C.append(correct)
        O.append(obj)
        success_rate = 100*len([c for c in C if c])/float(len(C))
        if opts.mode == 'aggressive':
            mode = " (+)"
        else:
            mode = ""
        print "%s-%s%s δ0: %5.3f, δ1: %5.3f, timeout: %.2f :: "%(opts.cipher, opts.size, mode, opts.delta0, opts.delta1, opts.timeout),
        print "i: %4d, min: %7.2f, avg: %7.2f, max %7.2f, success rate: %6.2f%%"%(i+1,min(T),sum(T)/len(T),max(T),success_rate)            

from sage.crypto.mq.sr import SR_gf2
class SR_gf2_lex(SR_gf2):
    def inversion_polynomials_single_sbox(self, x=None, w=None, biaffine_only=None, correct_only=None, groebner=False):
        e = self.e
        if x is None and w is None:
            # make sure it prints like in the book.
            names = ["w%d" % i for i in reversed(range(e))] + ["x%d"%i for i in reversed(range(e))]
            P = PolynomialRing(GF(2), e*2, names, order='lex')
            x = P.gens()[e:]
            w = P.gens()[:e]

        if e == 4:
            w3,w2,w1,w0 = w
            x3,x2,x1,x0 = x
            return [w3 + x3*x2*x1 + x3*x2 + x3*x1 + x3*x0 + x3 + x2 + x1,
                    w2 + x3*x2*x0 + x3*x0 + x3 + x2*x0 + x2 + x1*x0,
                    w1 + x3*x1*x0 + x3*x1 + x3 + x2*x1 + x2*x0 + x1*x0,
                    w0 + x3*x2*x1 + x3 + x2*x1*x0 + x2*x1 + x2*x0 + x2 + x1 + x0]
        else:
            x7,x6,x5,x4,x3,x2,x1,x0 = w
            w7,w6,w5,w4,w3,w2,w1,w0 = x

            return [x7 + w7*w6*w5*w4*w3*w2*w0 + w7*w6*w5*w4*w3*w0 + w7*w6*w5*w4*w0 + w7*w6*w5*w4 + w7*w6*w5*w3*w2*w1*w0 + w7*w6*w5*w3*w2*w0 + w7*w6*w5*w3*w2 + w7*w6*w5*w3*w1*w0 + w7*w6*w5*w3*w1 + w7*w6*w5*w3 + w7*w6*w5 + w7*w6*w4*w3*w2*w0 + w7*w6*w4*w3*w2 + w7*w6*w4*w2 + w7*w6*w4*w0 + w7*w6*w4 + w7*w6*w3*w2*w0 + w7*w6*w3*w1*w0 + w7*w6*w3*w0 + w7*w6*w3 + w7*w6 + w7*w5*w4*w3*w2*w1*w0 + w7*w5*w4*w3*w2*w1 + w7*w5*w4*w3*w1*w0 + w7*w5*w4*w3*w1 + w7*w5*w4*w3*w0 + w7*w5*w4*w2*w1 + w7*w5*w4*w2*w0 + w7*w5*w4*w2 + w7*w5*w4*w1*w0 + w7*w5*w4*w0 + w7*w5*w4 + w7*w5*w3*w2 + w7*w5*w3*w0 + w7*w5*w3 + w7*w5*w2*w1*w0 + w7*w5*w2 + w7*w5*w1*w0 + w7*w5*w1 + w7*w4*w3*w2*w1*w0 + w7*w4*w3*w2*w1 + w7*w4*w3*w2*w0 + w7*w4*w3*w1*w0 + w7*w4*w3*w1 + w7*w4*w2*w1*w0 + w7*w4*w1*w0 + w7*w4*w0 + w7*w3*w2*w1*w0 + w7*w3*w2*w0 + w7*w3*w2 + w7*w3*w1 + w7*w3 + w7*w2*w1 + w7*w2*w0 + w7*w2 + w7*w0 + w7 + w6*w5*w4*w3*w2*w0 + w6*w5*w4*w3*w2 + w6*w5*w4*w3*w1*w0 + w6*w5*w4*w3*w1 + w6*w5*w4*w3 + w6*w5*w4*w2*w1 + w6*w5*w4*w2*w0 + w6*w5*w4 + w6*w5*w3*w2*w1*w0 + w6*w5*w3*w1*w0 + w6*w5*w3*w1 + w6*w5*w3*w0 + w6*w5*w2*w1 + w6*w5*w2*w0 + w6*w5*w2 + w6*w5*w0 + w6*w4*w3*w2*w1*w0 + w6*w4*w3*w2*w0 + w6*w4*w3*w2 + w6*w4*w3*w1 + w6*w4*w3*w0 + w6*w4*w3 + w6*w4*w2*w1*w0 + w6*w4*w2*w1 + w6*w4*w2*w0 + w6*w4*w2 + w6*w4*w1*w0 + w6*w4*w1 + w6*w4*w0 + w6*w4 + w6*w3*w2*w1*w0 + w6*w3*w2*w1 + w6*w3*w1*w0 + w6*w3*w0 + w6*w2*w1 + w6*w2 + w6*w1*w0 + w6*w1 + w6*w0 + w5*w4*w3*w2*w1*w0 + w5*w4*w3*w2*w1 + w5*w4*w3*w2*w0 + w5*w4*w3*w1*w0 + w5*w4*w3 + w5*w4*w2*w1 + w5*w4*w2 + w5*w4*w1 + w5*w4*w0 + w5*w3*w2*w1*w0 + w5*w3*w2*w1 + w5*w3*w2*w0 + w5*w3*w1*w0 + w5*w2*w1*w0 + w5*w2*w1 + w5*w2*w0 + w5*w2 + w5*w1*w0 + w5*w1 + w4*w3*w2*w0 + w4*w3*w2 + w4*w3 + w4*w2*w1*w0 + w4*w2*w0 + w4*w0 + w3*w2*w1 + w3*w2 + w3*w0 + w3 + w2*w0 + w2 + w1,
                    x6 + w7*w6*w5*w4*w3*w1*w0 + w7*w6*w5*w4*w3*w1 + w7*w6*w5*w4*w3 + w7*w6*w5*w4*w2*w1 + w7*w6*w5*w4*w2*w0 + w7*w6*w5*w4*w2 + w7*w6*w5*w4*w1*w0 + w7*w6*w5*w4 + w7*w6*w5*w3*w2 + w7*w6*w5*w3*w1 + w7*w6*w5*w2*w1*w0 + w7*w6*w5*w2*w0 + w7*w6*w5*w1*w0 + w7*w6*w5 + w7*w6*w4*w3*w2*w1*w0 + w7*w6*w4*w3*w2*w1 + w7*w6*w4*w3*w1*w0 + w7*w6*w4*w2*w1*w0 + w7*w6*w4*w2*w1 + w7*w6*w4*w2*w0 + w7*w6*w4*w2 + w7*w6*w4*w1*w0 + w7*w6*w4*w1 + w7*w6*w3*w2 + w7*w6*w3*w1*w0 + w7*w6*w3*w1 + w7*w6*w3*w0 + w7*w6*w3 + w7*w6*w2*w0 + w7*w6*w1*w0 + w7*w6*w1 + w7*w5*w4*w3*w2*w1 + w7*w5*w4*w3*w2*w0 + w7*w5*w4*w3*w1*w0 + w7*w5*w4*w3*w1 + w7*w5*w4*w3*w0 + w7*w5*w4*w3 + w7*w5*w4*w2*w1*w0 + w7*w5*w4*w2*w0 + w7*w5*w4*w2 + w7*w5*w4*w1 + w7*w5*w4*w0 + w7*w5*w4 + w7*w5*w3*w2*w1*w0 + w7*w5*w3*w2*w1 + w7*w5*w3*w2 + w7*w5*w3*w1 + w7*w5*w3 + w7*w5*w2*w1 + w7*w5*w2*w0 + w7*w5*w2 + w7*w5*w1*w0 + w7*w5*w1 + w7*w5 + w7*w4*w3*w2*w1 + w7*w4*w3*w2*w0 + w7*w4*w3*w2 + w7*w4*w3*w1*w0 + w7*w4*w3*w0 + w7*w4*w2*w1 + w7*w4*w1 + w7*w3*w2*w1*w0 + w7*w3*w2 + w7*w3*w1*w0 + w7*w3*w0 + w7*w3 + w7*w2*w1*w0 + w7*w2*w1 + w7*w2 + w7*w1*w0 + w7*w1 + w7*w0 + w6*w5*w4*w3*w2*w1*w0 + w6*w5*w4*w3*w2*w1 + w6*w5*w4*w3*w2*w0 + w6*w5*w4*w3*w2 + w6*w5*w4*w3*w1 + w6*w5*w4*w3*w0 + w6*w5*w4*w2*w1*w0 + w6*w5*w4*w2*w1 + w6*w5*w4*w2*w0 + w6*w5*w4 + w6*w5*w3*w2*w0 + w6*w5*w3*w2 + w6*w5*w3*w0 + w6*w5*w3 + w6*w5*w2*w1*w0 + w6*w5*w2 + w6*w5*w1 + w6*w5*w0 + w6*w4*w3*w2*w1 + w6*w4*w3*w2*w0 + w6*w4*w3*w2 + w6*w4*w3*w1 + w6*w4*w2*w1*w0 + w6*w4*w2*w1 + w6*w4*w2*w0 + w6*w3*w2*w1 + w6*w3*w2 + w6*w3*w1*w0 + w6*w3*w1 + w6*w3 + w6*w2*w1*w0 + w6*w2*w1 + w6*w2*w0 + w6*w2 + w6*w1*w0 + w6*w0 + w5*w4*w3*w2*w1*w0 + w5*w4*w3*w1*w0 + w5*w4*w3*w1 + w5*w4*w3 + w5*w4*w0 + w5*w4 + w5*w3*w2*w1 + w5*w3*w1 + w5*w2*w1*w0 + w5*w2*w0 + w5*w1 + w5*w0 + w4*w3*w2*w1*w0 + w4*w3*w2*w0 + w4*w3*w2 + w4*w3*w1*w0 + w4*w3 + w4*w2*w1*w0 + w4*w2*w0 + w4*w1*w0 + w4*w1 + w4*w0 + w4 + w3*w2*w1*w0 + w3*w2*w0 + w3*w1 + w3 + w2*w1*w0 + w2 + w1*w0,
                    x5 + w7*w6*w5*w4*w3*w2*w1 + w7*w6*w5*w4*w3*w2*w0 + w7*w6*w5*w4*w3*w2 + w7*w6*w5*w4*w3*w1 + w7*w6*w5*w4*w3*w0 + w7*w6*w5*w4*w3 + w7*w6*w5*w4*w2 + w7*w6*w5*w4*w1*w0 + w7*w6*w5*w4*w1 + w7*w6*w5*w3*w2*w1*w0 + w7*w6*w5*w3*w2*w1 + w7*w6*w5*w3*w2 + w7*w6*w5*w3*w1 + w7*w6*w5*w3*w0 + w7*w6*w5*w2*w0 + w7*w6*w5 + w7*w6*w4*w3*w1 + w7*w6*w4*w3 + w7*w6*w4*w2*w0 + w7*w6*w4*w1*w0 + w7*w6*w4*w1 + w7*w6*w4 + w7*w6*w3*w2*w1*w0 + w7*w6*w3*w2*w1 + w7*w6*w3 + w7*w6*w2*w1*w0 + w7*w6*w2 + w7*w6*w1*w0 + w7*w6*w1 + w7*w6*w0 + w7*w5*w4*w3*w2*w1*w0 + w7*w5*w4*w3*w2 + w7*w5*w4*w3*w1 + w7*w5*w4*w3*w0 + w7*w5*w4*w3 + w7*w5*w4*w2 + w7*w5*w4*w1*w0 + w7*w5*w3*w2*w1 + w7*w5*w3*w2 + w7*w5*w3*w1 + w7*w5*w3*w0 + w7*w5*w2*w1*w0 + w7*w5*w2*w0 + w7*w5*w1*w0 + w7*w5*w0 + w7*w4*w3*w2 + w7*w4*w3*w0 + w7*w4*w3 + w7*w4*w2*w1*w0 + w7*w4*w2*w1 + w7*w4*w2 + w7*w4*w1*w0 + w7*w4 + w7*w3*w2*w1 + w7*w3*w2 + w7*w3*w1*w0 + w7*w3*w1 + w7*w3 + w7*w2*w1 + w7*w1*w0 + w7*w1 + w7*w0 + w6*w5*w4*w3*w2*w1 + w6*w5*w4*w3*w1*w0 + w6*w5*w4*w3*w0 + w6*w5*w4*w2*w1 + w6*w5*w4*w2*w0 + w6*w5*w4*w2 + w6*w5*w4*w0 + w6*w5*w4 + w6*w5*w3*w2*w0 + w6*w5*w3*w0 + w6*w5*w1 + w6*w5*w0 + w6*w5 + w6*w4*w3*w2*w1*w0 + w6*w4*w3*w2 + w6*w4*w3*w0 + w6*w4*w2*w1*w0 + w6*w4*w2 + w6*w3*w2*w1*w0 + w6*w3*w2*w1 + w6*w3*w2*w0 + w6*w3*w1*w0 + w6*w3*w1 + w6*w2*w1*w0 + w6*w2*w0 + w6*w2 + w6*w1 + w6*w0 + w5*w4*w3*w2*w1 + w5*w4*w3*w1*w0 + w5*w4*w3*w1 + w5*w4*w3*w0 + w5*w4*w3 + w5*w4*w2*w1 + w5*w4*w1*w0 + w5*w4*w0 + w5*w4 + w5*w3*w2*w1*w0 + w5*w3*w2*w1 + w5*w3*w1 + w5*w3*w0 + w5*w2*w1 + w5*w2 + w5*w1 + w5 + w4*w3*w2*w1*w0 + w4*w3*w2*w1 + w4*w3*w1 + w4*w2*w1*w0 + w4*w2*w0 + w4*w2 + w4 + w3*w2*w1*w0 + w3*w2*w1 + w3*w2*w0 + w3*w1*w0 + w3*w0 + w3 + w2*w1 + w1*w0,
                    x4 + w7*w6*w5*w4*w3*w2*w0 + w7*w6*w5*w4*w3*w2 + w7*w6*w5*w4*w3*w1*w0 + w7*w6*w5*w4*w2*w1 + w7*w6*w5*w4*w1 + w7*w6*w5*w4*w0 + w7*w6*w5*w3*w2 + w7*w6*w5*w3*w1*w0 + w7*w6*w5*w3*w1 + w7*w6*w5*w3 + w7*w6*w5*w2*w0 + w7*w6*w5*w1 + w7*w6*w5*w0 + w7*w6*w5 + w7*w6*w4*w3*w2*w1*w0 + w7*w6*w4*w3*w1 + w7*w6*w4*w2*w1*w0 + w7*w6*w4*w2*w0 + w7*w6*w4*w1 + w7*w6*w3*w2*w1*w0 + w7*w6*w3*w0 + w7*w6*w2*w1*w0 + w7*w6*w2*w0 + w7*w6*w2 + w7*w6*w1 + w7*w6*w0 + w7*w6 + w7*w5*w4*w3*w2*w1 + w7*w5*w4*w3*w2*w0 + w7*w5*w4*w3 + w7*w5*w4*w2*w1*w0 + w7*w5*w4*w1 + w7*w5*w4*w0 + w7*w5*w3*w2*w1*w0 + w7*w5*w3*w2*w1 + w7*w5*w3*w1*w0 + w7*w5*w3 + w7*w5*w2*w1*w0 + w7*w5*w2*w0 + w7*w4*w3*w2*w1*w0 + w7*w4*w3*w2*w1 + w7*w4*w3*w2*w0 + w7*w4*w3*w2 + w7*w4*w3*w1 + w7*w4*w3*w0 + w7*w4*w2*w1 + w7*w4*w2*w0 + w7*w4*w2 + w7*w4*w1*w0 + w7*w3*w2*w1*w0 + w7*w3*w2*w1 + w7*w3*w1*w0 + w7*w3*w1 + w7*w3*w0 + w7*w3 + w7*w2 + w7*w1*w0 + w7*w1 + w7*w0 + w6*w5*w4*w3*w2*w1*w0 + w6*w5*w4*w3*w2*w0 + w6*w5*w4*w3*w2 + w6*w5*w4*w3*w0 + w6*w5*w4*w2*w1*w0 + w6*w5*w4*w2*w1 + w6*w5*w4*w2 + w6*w5*w4*w1*w0 + w6*w5*w4*w1 + w6*w5*w4 + w6*w5*w3*w2*w1*w0 + w6*w5*w3*w2 + w6*w5*w2*w1*w0 + w6*w5*w2*w1 + w6*w5*w1 + w6*w5*w0 + w6*w5 + w6*w4*w3*w2*w1 + w6*w4*w3*w2 + w6*w4*w3*w0 + w6*w4*w2*w1*w0 + w6*w4*w2 + w6*w4*w1*w0 + w6*w4*w1 + w6*w4*w0 + w6*w3*w2 + w6*w3*w1*w0 + w6*w3 + w6*w2*w1*w0 + w6*w2 + w6 + w5*w4*w3*w2*w1 + w5*w4*w3*w2*w0 + w5*w4*w3*w2 + w5*w4*w3*w0 + w5*w4*w2*w1*w0 + w5*w4*w2 + w5*w4*w1*w0 + w5*w4*w0 + w5*w3*w2*w1 + w5*w3*w2*w0 + w5*w3*w1 + w5*w3*w0 + w5*w3 + w5*w2*w0 + w5*w0 + w5 + w4*w3*w2*w1 + w4*w3*w2 + w4*w3*w1 + w4*w2*w1 + w4*w1*w0 + w4*w1 + w4 + w3*w2*w1*w0 + w3*w2 + w3*w1*w0 + w2*w1 + w2*w0 + w1*w0,
                    x3 + w7*w6*w5*w4*w3*w2*w1 + w7*w6*w5*w4*w3*w1*w0 + w7*w6*w5*w4*w3*w1 + w7*w6*w5*w4*w3 + w7*w6*w5*w4*w1 + w7*w6*w5*w4 + w7*w6*w5*w3*w2*w1 + w7*w6*w5*w3*w1*w0 + w7*w6*w5*w3*w1 + w7*w6*w5*w2*w1*w0 + w7*w6*w5*w2*w1 + w7*w6*w5*w2*w0 + w7*w6*w5*w2 + w7*w6*w5*w0 + w7*w6*w4*w3*w2*w1 + w7*w6*w4*w3*w2 + w7*w6*w4*w3*w0 + w7*w6*w4*w3 + w7*w6*w4*w2*w0 + w7*w6*w4*w2 + w7*w6*w4 + w7*w6*w3*w2*w1*w0 + w7*w6*w3*w2*w1 + w7*w6*w3*w2*w0 + w7*w6*w3*w2 + w7*w6*w3 + w7*w6*w2*w1*w0 + w7*w6*w2 + w7*w6*w1 + w7*w6*w0 + w7*w5*w4*w3*w2*w1 + w7*w5*w4*w3*w2*w0 + w7*w5*w4*w3*w2 + w7*w5*w4*w3*w1*w0 + w7*w5*w4*w3*w1 + w7*w5*w4*w3*w0 + w7*w5*w4*w3 + w7*w5*w4*w2*w1*w0 + w7*w5*w4*w2*w1 + w7*w5*w4*w2 + w7*w5*w4*w1*w0 + w7*w5*w4*w1 + w7*w5*w4*w0 + w7*w5*w4 + w7*w5*w3*w2*w1 + w7*w5*w3*w2 + w7*w5*w3*w1*w0 + w7*w5*w3*w0 + w7*w5*w2*w1 + w7*w5*w2*w0 + w7*w5*w1*w0 + w7*w5*w0 + w7*w4*w3*w2*w1 + w7*w4*w3*w1*w0 + w7*w4*w3*w1 + w7*w4*w3 + w7*w4*w2*w1 + w7*w4*w0 + w7*w4 + w7*w3*w2*w1*w0 + w7*w3*w2*w1 + w7*w3*w2 + w7*w3*w1*w0 + w7*w3*w1 + w7*w3*w0 + w7*w2*w1 + w7*w2 + w7*w1*w0 + w7*w0 + w6*w5*w4*w2*w1 + w6*w5*w4*w2*w0 + w6*w5*w4*w1 + w6*w5*w4*w0 + w6*w5*w4 + w6*w5*w3*w2*w1 + w6*w5*w3*w1*w0 + w6*w5*w3*w1 + w6*w5*w3 + w6*w5*w2*w0 + w6*w5*w2 + w6*w5*w1 + w6*w5*w0 + w6*w4*w3*w2*w0 + w6*w4*w3 + w6*w4*w2*w1*w0 + w6*w4*w2*w1 + w6*w4*w2*w0 + w6*w3*w2*w1*w0 + w6*w3*w2*w1 + w6*w3*w1*w0 + w6*w3*w1 + w6*w2*w1*w0 + w6*w2*w1 + w6*w2 + w6*w1*w0 + w6 + w5*w4*w3*w2*w1 + w5*w4*w3*w2*w0 + w5*w4*w3*w2 + w5*w4*w2*w1*w0 + w5*w4*w2*w1 + w5*w4*w1 + w5*w4*w0 + w5*w3*w2*w1 + w5*w3*w2*w0 + w5*w3*w2 + w5*w3*w1*w0 + w5*w3*w0 + w5*w2*w1*w0 + w5*w1 + w5 + w4*w3*w2*w1*w0 + w4*w3*w2*w1 + w4*w3*w2 + w4*w2*w1 + w4*w2*w0 + w4*w1*w0 + w3*w2*w1 + w3*w2*w0 + w3*w1 + w3 + w2*w1*w0 + w2*w1 + w2*w0 + w2 + w1*w0 + w1,
                    x2 + w7*w6*w5*w4*w3*w0 + w7*w6*w5*w4*w2*w1*w0 + w7*w6*w5*w4*w2*w0 + w7*w6*w5*w4 + w7*w6*w5*w3*w2*w1*w0 + w7*w6*w5*w3*w1*w0 + w7*w6*w5*w3*w0 + w7*w6*w5*w3 + w7*w6*w5*w2*w1*w0 + w7*w6*w5*w2*w0 + w7*w6*w5*w2 + w7*w6*w5*w1*w0 + w7*w6*w5*w1 + w7*w6*w4*w2*w1*w0 + w7*w6*w4*w2*w1 + w7*w6*w4*w2*w0 + w7*w6*w4*w0 + w7*w6*w3*w2*w1*w0 + w7*w6*w3*w2*w0 + w7*w6*w3*w1 + w7*w6*w2*w1*w0 + w7*w6*w2 + w7*w6*w1*w0 + w7*w6*w1 + w7*w6*w0 + w7*w6 + w7*w5*w4*w3*w2*w1*w0 + w7*w5*w4*w3*w2*w1 + w7*w5*w4*w3*w2*w0 + w7*w5*w4*w3*w1*w0 + w7*w5*w4*w3*w0 + w7*w5*w4*w2*w1 + w7*w5*w4*w2 + w7*w5*w4*w1*w0 + w7*w5*w4*w0 + w7*w5*w3*w2*w1 + w7*w5*w3*w1 + w7*w5*w3 + w7*w5*w2*w0 + w7*w5*w2 + w7*w5*w1*w0 + w7*w5*w1 + w7*w5*w0 + w7*w4*w3*w2 + w7*w4*w3*w1 + w7*w4*w2*w1*w0 + w7*w4*w2*w1 + w7*w4*w2*w0 + w7*w4*w2 + w7*w3*w2*w1 + w7*w3*w2*w0 + w7*w3*w1*w0 + w7*w3*w1 + w7*w3*w0 + w7*w2*w1*w0 + w7*w2 + w7*w0 + w6*w5*w4*w3*w0 + w6*w5*w4*w2*w1*w0 + w6*w5*w4*w2*w1 + w6*w5*w4*w2*w0 + w6*w5*w4*w1*w0 + w6*w5*w4 + w6*w5*w3*w2*w1 + w6*w5*w3*w2*w0 + w6*w5*w3*w2 + w6*w5*w3*w1*w0 + w6*w5*w3*w1 + w6*w5*w2*w1 + w6*w5*w2*w0 + w6*w5*w1 + w6*w5*w0 + w6*w4*w3*w2*w1*w0 + w6*w4*w3*w0 + w6*w4*w2*w0 + w6*w4*w2 + w6*w4*w1*w0 + w6*w4 + w6*w3*w2*w1*w0 + w6*w3*w1*w0 + w6*w3*w0 + w6*w2*w1 + w6*w2*w0 + w6*w1 + w6 + w5*w4*w3*w2*w1*w0 + w5*w4*w3*w2*w0 + w5*w4*w3*w2 + w5*w4*w3*w1*w0 + w5*w4*w2*w1 + w5*w4*w2*w0 + w5*w4*w2 + w5*w4*w1 + w5*w4*w0 + w5*w3*w2*w1 + w5*w3*w2*w0 + w5*w3*w2 + w5*w3*w1*w0 + w5*w3*w1 + w5*w2*w1*w0 + w5*w2*w0 + w5*w2 + w5*w1*w0 + w5*w1 + w5*w0 + w4*w3*w2*w1*w0 + w4*w3*w2*w0 + w4*w3*w1 + w4*w3 + w4*w2*w1*w0 + w4*w2 + w4 + w3*w2*w1*w0 + w3*w2*w0 + w3*w1*w0 + w3*w1 + w3*w0 + w2*w1 + w1,
                    x1 + w7*w6*w5*w4*w1*w0 + w7*w6*w5*w4*w1 + w7*w6*w5*w3*w2*w1*w0 + w7*w6*w5*w3*w2*w1 + w7*w6*w5*w3*w2 + w7*w6*w5*w3*w1*w0 + w7*w6*w5*w3*w1 + w7*w6*w5*w2*w1 + w7*w6*w5*w0 + w7*w6*w5 + w7*w6*w4*w3*w2*w1*w0 + w7*w6*w4*w3*w2*w0 + w7*w6*w4*w3*w2 + w7*w6*w4*w3*w1 + w7*w6*w4*w3*w0 + w7*w6*w4*w3 + w7*w6*w4*w2*w1 + w7*w6*w4*w2 + w7*w6*w3*w2*w0 + w7*w6*w3*w2 + w7*w6*w3*w1 + w7*w6*w3*w0 + w7*w6*w2*w1*w0 + w7*w6*w2*w0 + w7*w6*w2 + w7*w6*w1 + w7*w5*w4*w3*w2*w1 + w7*w5*w4*w3*w2*w0 + w7*w5*w4*w2*w1*w0 + w7*w5*w4*w2*w0 + w7*w5*w4*w1 + w7*w5*w4*w0 + w7*w5*w3*w2*w0 + w7*w5*w3*w1*w0 + w7*w5*w3*w1 + w7*w5*w3 + w7*w5*w2*w1 + w7*w5*w0 + w7*w5 + w7*w4*w3*w2*w1*w0 + w7*w4*w3*w2*w1 + w7*w4*w3*w1*w0 + w7*w4*w3*w0 + w7*w4*w2*w1 + w7*w4*w1*w0 + w7*w4*w1 + w7*w3*w2*w1*w0 + w7*w3*w2*w0 + w7*w3*w2 + w7*w3*w1*w0 + w7*w3*w1 + w7*w3*w0 + w7*w2*w1*w0 + w7*w2 + w7 + w6*w5*w4*w3*w2*w1*w0 + w6*w5*w4*w3*w2*w1 + w6*w5*w4*w3*w2*w0 + w6*w5*w4*w3*w1*w0 + w6*w5*w4*w3*w1 + w6*w5*w4*w3 + w6*w5*w4*w2*w1*w0 + w6*w5*w4*w2*w1 + w6*w5*w4*w2*w0 + w6*w5*w4*w1*w0 + w6*w5*w3*w2 + w6*w5*w3*w1 + w6*w5*w3*w0 + w6*w5*w3 + w6*w5*w2*w1*w0 + w6*w5*w2 + w6*w5*w1*w0 + w6*w5*w1 + w6*w5*w0 + w6*w4*w3*w2*w0 + w6*w4*w3*w2 + w6*w4*w3*w1 + w6*w4*w3*w0 + w6*w4*w3 + w6*w4*w2*w1 + w6*w4*w2 + w6*w4*w1*w0 + w6*w4*w0 + w6*w3*w2*w1 + w6*w3*w2*w0 + w6*w3*w1 + w6*w3*w0 + w6*w3 + w6*w2*w1*w0 + w6*w2*w1 + w6*w2*w0 + w6*w2 + w6*w1 + w6*w0 + w5*w4*w3*w2*w1 + w5*w4*w3*w2*w0 + w5*w4*w3*w1 + w5*w4*w2*w1*w0 + w5*w4*w2 + w5*w4*w1*w0 + w5*w4 + w5*w3*w2*w1 + w5*w3 + w5*w2*w1*w0 + w5 + w4*w3*w2*w1 + w4*w3*w1*w0 + w4*w3*w1 + w4*w2*w1 + w4*w2*w0 + w4*w2 + w4*w1*w0 + w4*w1 + w3*w2*w0 + w3*w2 + w3*w0 + w2 + w1*w0,
                    x0 + w7*w6*w5*w4*w3*w2*w1 + w7*w6*w5*w4*w3*w2 + w7*w6*w5*w4*w3*w1 + w7*w6*w5*w4*w2*w1*w0 + w7*w6*w5*w4*w2*w1 + w7*w6*w5*w4*w2*w0 + w7*w6*w5*w4*w2 + w7*w6*w5*w4*w1*w0 + w7*w6*w5*w3*w2*w1 + w7*w6*w5*w3 + w7*w6*w5*w2*w1 + w7*w6*w5*w2*w0 + w7*w6*w5*w0 + w7*w6*w5 + w7*w6*w4*w3*w2*w1*w0 + w7*w6*w4*w3*w2*w0 + w7*w6*w4*w3*w2 + w7*w6*w4*w3*w0 + w7*w6*w4*w3 + w7*w6*w4*w1 + w7*w6*w3*w2*w1*w0 + w7*w6*w3*w2*w1 + w7*w6*w3*w2*w0 + w7*w6*w3*w1*w0 + w7*w6*w3*w0 + w7*w6*w2*w1*w0 + w7*w6*w2*w1 + w7*w6*w2 + w7*w6*w1 + w7*w6 + w7*w5*w4*w3*w2*w1 + w7*w5*w4*w3*w2*w0 + w7*w5*w4*w3 + w7*w5*w4*w2*w1*w0 + w7*w5*w4*w2*w1 + w7*w5*w4*w2*w0 + w7*w5*w4*w1 + w7*w5*w3*w2*w1*w0 + w7*w5*w2*w1 + w7*w5*w2 + w7*w5*w1*w0 + w7*w5*w0 + w7*w4*w3*w2*w1*w0 + w7*w4*w3*w2*w1 + w7*w4*w3*w2*w0 + w7*w4*w3*w2 + w7*w4*w3*w0 + w7*w4*w2*w1*w0 + w7*w4*w2 + w7*w4*w1 + w7*w4 + w7*w3*w2*w1 + w7*w3*w1*w0 + w7*w2*w1 + w7*w1 + w7 + w6*w5*w4*w3 + w6*w5*w4*w2*w1 + w6*w5*w4*w2*w0 + w6*w5*w4*w2 + w6*w5*w4 + w6*w5*w3*w2*w1 + w6*w5*w3*w1 + w6*w5*w3 + w6*w5*w2 + w6*w5 + w6*w4*w3*w2*w1*w0 + w6*w4*w3*w2*w0 + w6*w4*w3*w1*w0 + w6*w4*w3*w1 + w6*w4*w3 + w6*w4*w2*w1 + w6*w4*w2 + w6*w4*w1 + w6*w4*w0 + w6*w3*w2*w1*w0 + w6*w3*w2*w0 + w6*w2*w1 + w6*w2*w0 + w6*w2 + w6*w1*w0 + w6*w1 + w6 + w5*w4*w3*w2*w1 + w5*w4*w3*w2*w0 + w5*w4*w3*w2 + w5*w4*w3*w1*w0 + w5*w4*w3 + w5*w4*w2*w0 + w5*w4*w1*w0 + w5*w4*w1 + w5*w3*w2*w1 + w5*w3*w2*w0 + w5*w3*w2 + w5*w3*w1*w0 + w5*w3*w1 + w5*w3*w0 + w5*w3 + w5*w2*w1*w0 + w5*w1*w0 + w5*w1 + w5*w0 + w4*w3*w2*w1*w0 + w4*w3*w2 + w4*w3*w1*w0 + w4*w3*w1 + w4*w3*w0 + w4*w2*w1*w0 + w4*w1*w0 + w4*w1 + w4*w0 + w3*w2*w1 + w3*w2 + w2*w1*w0 + w2*w1 + w2 + w1 + w0]

from anf2mip import ProbabilisticPolynomialSequence 
def test_aes_mip(n=1,r=1,c=1,e=4, delta0=0.15, delta1=0.001, conversion='IASC',solver=None, coldboot=True, write_out=False, timeout=None, parameters=None):
 
    sr = SR_gf2_lex(n,r,c,e, gf2=True, polybori=True, allow_zero_inversions=True, correct_only=True, biaffine_only=True)
    K = sr.random_state_array()
    K0 = copy(K)
    hard = []
    soft = []
    for i in range(n+1):
        kv = sr.vars("k",i)
        K = sr.key_schedule(K,i)
        hard.extend(sr.key_schedule_polynomials(i))
        soft.extend(map(add,zip(sr.phi(K).transpose().list(),kv)))

    for i,f in enumerate(soft):
        if coldboot:
            if random() <= delta0 and soft[i].constant_coefficient() == 1:
                soft[i] += 1
            if random() <= delta1 and soft[i].constant_coefficient() == 0:
                soft[i] += 1
        else:
            if random() <= delta0:
                soft[i] += 1

    if coldboot == 'aggressive':
        hard.extend([f for f in soft if f.constant_coefficient() == 1])
        soft = [f for f in soft if f.constant_coefficient() == 0]

    F = ProbabilisticPolynomialSequence(sr.ring(),hard,soft)
    
    enforce_boolean = True

    if not delta1 or not delta0:
        dd = 1
    else:
        dd = int(delta0/delta1)

    def weight_callback(f):
        if f.constant_coefficient() == 1:
            return dd # the probability that this is wrong is
                            # small, so the penalty is big
        else:
            return 1

    if write_out:
        if write_out is not True:
            s = "-"+str(write_out)
        else:
            s = ""
        if coldboot:
            cbs = "-coldboot"
        else:
            cbs = ""
        F.write_mip("/home/malb/AES-%d-%d-%d-%d-%s-%4.2f%s%s.mps"%(n,r,c,e,conversion,delta0,cbs,s),
                    filetype='mps',
                    conversion=conversion,
                    enforce_boolean=enforce_boolean,
                    weight_callback=weight_callback)
        return

    t = walltime()
    if solver == 'SCIP2':
        S = F.solve_scip(weight_callback=weight_callback,
                         timeout=timeout,
                         parameters=parameters)

    else:
        S = F.solve_mip(conversion=conversion,
                        solver=solver,
                        enforce_boolean=enforce_boolean,
                        weight_callback=weight_callback,
                        timeout=timeout)

    t = walltime(t)
    if S is None:
        return t,-1.0,False

    for s,obj in S:
        correct = True
        for i,v in enumerate(sr.vars("k",0)):
            try:
                if s[v]  != sr.phi(K0).transpose().list()[i]:
                    correct = False
                    break
            except KeyError:
                pass
        if correct:
            return t,obj,correct
    return  t,-2.0,False
	
if __name__ == '__main__':
    main()
